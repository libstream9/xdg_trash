#ifndef STREAM9_TRASH_ENTRY_HPP
#define STREAM9_TRASH_ENTRY_HPP

#include "file_time.hpp"
#include "json_fwd.hpp"
#include "namespace.hpp"

#include <stream9/string.hpp>

namespace stream9::xdg::trash {

class trash_info
{
public:
    // essential

    // Construct by loading trashinfo file.
    // Throw error if there is something wrong with the file.
    trash_info(string_view trash_info_path);

    // Create trashinfo file in the directory 'dir' with 'original' as
    // path to the deleted file and current time as deletion date.
    // The filename part of 'original' will be used as trashinfo filename,
    // but if it is already taken, make up alternative unique name (like 'taken (1)').
    //
    // post-condition: valid trashinfo file in 'dir'
    trash_info(string_view info_dir_path, string_view original_path);

    // query
    string_view name() const noexcept { return m_name; }
    string      info_path() const;
    string      file_path() const;

    // @return absolute path to the original location of deleted file
    //         or the relative path from the directory in which trash directory
    //         resides (for example, $XDG_DATA_HOME for "home trash" directory)
    string_view original_path() const noexcept { return m_original_path; }

    file_time_t deletion_date() const noexcept { return m_deletion_date; }
    file_time_t time_stamp() const noexcept { return m_time_stamp; }

    // modifier
    bool reload();

    json::object check_integrity();

private:
    friend class impl;

    using deletion_date_t = file_time<seconds>;

    string m_trash_dir_path; // absolute, normal
    string m_name; // non-empty
    string m_original_path; // normal, non-empty
    deletion_date_t m_deletion_date;
    file_time_t m_time_stamp;
};

void tag_invoke(json::value_from_tag, json::value&, trash_info const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_ENTRY_HPP
