#ifndef STREAM9_TRASH_TRASH_INFO_PARSER_HPP
#define STREAM9_TRASH_TRASH_INFO_PARSER_HPP

#include "error.hpp"
#include "file_time.hpp"

#include <system_error>

#include <stream9/function_ref.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::trash {

namespace trash_info_parser {

    using deletion_date_t = file_time<seconds>;

    struct fields {
        string_view path;
        deletion_date_t deletion_date;
    };

    using line_no_t = stream9::safe_integer<int, 0>;
    using column_t = stream9::safe_integer<int, 0>;
    using warning_cb =
        function_ref<void(std::error_code const&, line_no_t, column_t)>;

    /*
     * entry point
     */
    fields parse(string_view text, warning_cb);

    enum class errc {
        // error
        no_trash_info_group = 100,
        no_path_entry,
        no_deletion_date_entry,
        empty_path_entry,
        invalid_time_string,
        // warining
        empty_deletion_date_entry = 200,
        multiple_trash_info_group,
        non_grouped_entry,
        multiple_path_entry,
        multiple_deletion_date_entry,
    };

    std::error_category& error_category();

    inline std::error_code
    make_error_code(errc const e)
    {
        return { static_cast<int>(e), error_category() };
    }

} // namespace trash_info_parser

} // namespace stream9::xdg::trash

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::trash::trash_info_parser::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_TRASH_TRASH_INFO_PARSER_HPP
