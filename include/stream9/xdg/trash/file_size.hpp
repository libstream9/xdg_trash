#ifndef STREAM9_TRASH_FILE_SIZE_HPP
#define STREAM9_TRASH_FILE_SIZE_HPP

#include <cstdint>

#include <stream9/safe_integer.hpp>

namespace stream9::xdg::trash {

using file_size_t = safe_integer<std::int64_t, 0>;

constexpr file_size_t
operator"" _kib(unsigned long long int n) noexcept
{
    return n * 1024;
}

constexpr file_size_t
operator"" _mib(unsigned long long int n) noexcept
{
    return n * 1024 * 1024;
}

constexpr file_size_t
operator"" _gib(unsigned long long int n) noexcept
{
    return n * 1024 * 1024 * 1024;
}

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_FILE_SIZE_HPP
