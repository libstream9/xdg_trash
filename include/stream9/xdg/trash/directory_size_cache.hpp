#ifndef STREAM9_TRASH_DIRECTORY_SIZE_CACHE_HPP
#define STREAM9_TRASH_DIRECTORY_SIZE_CACHE_HPP

#include "environment.hpp"
#include "file_size.hpp"
#include "file_time.hpp"
#include "json_fwd.hpp"
#include "namespace.hpp"

#include <cstdint>
#include <functional>

#include <stream9/cstring_view.hpp>
#include <stream9/nonmovable.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/unique_array.hpp>

namespace stream9::xdg::trash {

class trash_directory;
class trash_info;

/*
 * @model !std::movable
 * @model !std::copyable
 * @model std::ranges::random_access_range
 */
class directory_size_cache : public rng::range_facade<directory_size_cache>
                           , public environment::directory_observer
                           , nonmovable
{
public:
    struct entry {
        using mtime_t = file_time<milliseconds>;
        string filename;
        file_size_t size;
        mtime_t mtime;

        bool operator==(entry const&) const = default;
    };

    using entry_set_t = unique_array<entry, std::less<>, project<&entry::filename>>;
    using const_iterator = entry_set_t::const_iterator;
    using iterator = entry_set_t::iterator;
    using size_type = safe_integer<std::int64_t, 0>;

public:
    // essential
    static shared_node<directory_size_cache>
        construct(trash_directory const&);

    ~directory_size_cache() noexcept = default;

    directory_size_cache(directory_size_cache const&) = delete;
    directory_size_cache& operator=(directory_size_cache const&) = delete;

    directory_size_cache(directory_size_cache&&) = delete;
    directory_size_cache& operator=(directory_size_cache&&) = delete;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    file_time_t last_modified_time() const noexcept;

    // query
    size_type size() const noexcept;

    const_iterator find(string_view filename) const noexcept;
    iterator       find(string_view filename) noexcept;

    cstring_view path() const noexcept;

    // modify
    bool insert_or_update_entry(trash_info const&);
    bool update_or_insert_entry(trash_info const&);

    void erase_entry(trash_info const&);

    void refresh();

    void clear() noexcept;

    json::object check_integrity();

protected:
    directory_size_cache(trash_directory const&);

private: // environment::directory_observer
    void file_created(string_view dirname, string_view filename) noexcept override;
    void file_modified(string_view dirname, string_view filename) noexcept override;
    void file_deleted(string_view dirname, string_view filename) noexcept override;

private:
    trash_directory const& m_trash_dir; // stable
    string m_path;
    file_time_t m_time_stamp;
    entry_set_t m_entries;
};

void tag_invoke(json::value_from_tag, json::value&, directory_size_cache::entry const&);
void tag_invoke(json::value_from_tag, json::value&, directory_size_cache const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_DIRECTORY_SIZE_CACHE_HPP
