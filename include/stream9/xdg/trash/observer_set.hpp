#ifndef STREAM9_TRASH_OBSERVER_SET_HPP
#define STREAM9_TRASH_OBSERVER_SET_HPP

#include "error.hpp"

#include <stream9/find.hpp>
#include <stream9/log.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/weak_node.hpp>

namespace stream9::xdg::trash {

/*
 * @model std::semiregular
 */
template<typename T>
class observer_set
{
public:
    using set_type = unique_array<weak_node<T>,
                            std::owner_less<weak_node<T>> >;

public:
    // essential
    observer_set() = default;

    // query
    auto size() const noexcept { return m_set.size(); }

    // modifier
    void insert(weak_node<T> o)
    {
        try {
            m_set.insert(o);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void erase(weak_node<T> o) noexcept
    {
        m_set.erase(find(m_set, o));
    }

    // command
    template<typename F>
        requires std::invocable<F, T&>
    void for_each(F func) noexcept
    {
        auto it = m_set.begin();
        while (it != m_set.end()) {
            if (auto o_node = (*it).lock()) {
                try {
                    func(**o_node);
                }
                catch (...) {
                    print_error(log::warn());
                }

                ++it;
            }
            else {
                it = m_set.erase(it);
            }
        }
    }

private:
    set_type m_set;
};

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_OBSERVER_SET_HPP
