#ifndef STREAM9_TRASH_TRASH_ENTRY_HPP
#define STREAM9_TRASH_TRASH_ENTRY_HPP

#include "file_size.hpp"
#include "file_time.hpp"
#include "namespace.hpp"

#include <stream9/cstring_view.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::trash {

class trash_directory;
class trash_info;

/*
 * @model std::copyable
 * @model std::equality_comparable
 */
class trash_entry
{
public:
    // essential
    trash_entry(string name,
                class trash_directory&,
                class trash_info&);

    // accessor
    cstring_view name() const noexcept { return m_name; }

    class trash_directory& trash_directory() const noexcept { return *m_trash_dir; }

    class trash_info& trash_info() const noexcept { return *m_info; }

    // query
    file_size_t file_size() const;

    file_time_t last_modified_time() const;

    file_time_t deletion_time() const noexcept;

    // comparison
    bool operator==(trash_entry const&) const noexcept;

private:
    string m_name;
    class trash_directory* m_trash_dir; // non-null, stable
    class trash_info* m_info; // non-null, stable
};

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TRASH_ENTRY_HPP
