#ifndef STREAM9_TRASH_TOP_DIRECTORY_SET_HPP
#define STREAM9_TRASH_TOP_DIRECTORY_SET_HPP

#include "environment.hpp"
#include "json_fwd.hpp"
#include "namespace.hpp"
#include "observer_set.hpp"
#include "top_directory.hpp"

#include <cstddef>
#include <ranges>

#include <stream9/array.hpp>
#include <stream9/indirect.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::trash {

/*
 * @model !std::copyable
 * @model !std::movable
 * @model std::ranges::random_access_range
 */
class top_directory_set : public rng::range_facade<top_directory_set>
                        , public environment::mount_observer
                        , public top_directory::observer
                        , public enable_shared_from_this<top_directory_set>
{
public:
    class observer
    {
    public:
        virtual ~observer() = default;

        virtual void top_directory_added(top_directory&) {}
        virtual void top_directory_about_to_be_removed(top_directory&) {}
    };

    using directory_set = array<shared_node<top_directory>>;

    using iterator = iter::indirect_iterator<rng::iterator_t<directory_set>>;
    using const_iterator = iter::indirect_iterator<rng::iterator_t<directory_set const>>;

public:
    // essential
    static shared_node<top_directory_set>
        construct();

    top_directory_set(top_directory_set const&) = delete;
    top_directory_set& operator=(top_directory_set const&) = delete;

    top_directory_set(top_directory_set&&) = delete;
    top_directory_set& operator=(top_directory_set&&) = delete;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    iterator begin() noexcept;
    iterator end() noexcept;

    // query
    auto size() const noexcept { return m_top_dirs.size(); }

    /*
     * find top_directory of filesystem which contains `p`
     */
    const_iterator find(string_view pathname) const noexcept;
    iterator       find(string_view pathname) noexcept;

    const_iterator find(trash_directory const&) const;

    // modifier
    iterator insert(string_view pathname);
    iterator erase(string_view pathname);

    void add_observer(weak_node<observer>);
    void remove_observer(weak_node<observer>);

protected:
    top_directory_set();

private:
    // environment::mount_observer
    void filesystem_mounted(string_view dir_path) noexcept override;
    void filesystem_unmounted(string_view dir_path) noexcept override;

    // top_directory::observer
    void directory_disappeared(top_directory&) override;

private:
    friend struct impl;

    directory_set m_top_dirs;
    observer_set<top_directory_set::observer> m_observers;
};

void tag_invoke(json::value_from_tag,
                json::value&, top_directory_set const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TOP_DIRECTORY_SET_HPP
