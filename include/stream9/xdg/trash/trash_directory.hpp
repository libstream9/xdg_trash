#ifndef STREAM9_TRASH_TRASH_DIRECTORY_HPP
#define STREAM9_TRASH_TRASH_DIRECTORY_HPP

#include "directory_settings.hpp"
#include "directory_size_cache.hpp"
#include "environment.hpp"
#include "file_size.hpp"
#include "json_fwd.hpp"
#include "namespace.hpp"
#include "observer_set.hpp"
#include "trash_info.hpp"

#include <compare>
#include <cstddef>
#include <functional>
#include <system_error>

#include <stream9/cstring_view.hpp>
#include <stream9/indirect.hpp>
#include <stream9/node.hpp>
#include <stream9/projection.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/unique_array.hpp>
#include <stream9/weak_node.hpp>

namespace stream9::xdg::trash {

class trash_directory;

/*
 * @model !std::copyable
 * @model !std::movable
 * @model std::ranges::random_access_range
 * @model std::totally_ordered
 */
class trash_directory : public rng::range_facade<trash_directory>
                      , public environment::directory_observer
{
public:
    using entry_set = unique_array<
                            node<trash_info>,
                            less,
                            indirect<project<&trash_info::name>> >;
    using iterator = iter::indirect_iterator<entry_set::iterator>;
    using const_iterator =
                     iter::const_indirect_iterator<entry_set::const_iterator>;
    using size_type = safe_integer<std::int64_t, 0>;

    class observer
    {
    public:
        virtual ~observer() = default;

        virtual void trash_info_created(trash_directory&, trash_info&) {}
        virtual void trash_info_deleted(trash_directory&, trash_info const&) {}
        virtual void trash_info_modified(trash_directory&, trash_info&) {}
        virtual void directory_disappeared(trash_directory&) {}
    };

    struct option {
        bool create_if_not_exist : 1 = false;
    };

public:
    // essentials
    static shared_node<trash_directory>
        construct(string_view path);

    static shared_node<trash_directory>
        construct(string_view path, option);

    trash_directory(trash_directory const&) = delete;
    trash_directory& operator=(trash_directory const&) = delete;

    trash_directory(trash_directory&&) = delete;
    trash_directory& operator=(trash_directory&&) = delete;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    iterator begin() noexcept;
    iterator end() noexcept;

    cstring_view path() const noexcept { return m_path; }

    class directory_size_cache const&
        directory_size_cache() const noexcept { return *m_dir_sizes; }

    // query
    size_type size() const noexcept;

    file_size_t file_size(const_iterator) const;
    file_size_t file_size(trash_info const&) const;

    file_size_t directory_size() const;

    file_size_t directory_size_limit() const noexcept;

    const_iterator find_info(trash_info const&) const noexcept;

    const_iterator find_info_by_name(string_view) const noexcept;

    const_iterator find_info_by_path(string_view) const noexcept;
    iterator find_info_by_path(string_view) noexcept;

    // modifier
    trash_info& trash_file(string_view file_path);

    void restore_trash(const_iterator);
    void restore_trash(trash_info const&);

    iterator delete_trash(const_iterator);
    void delete_trash(trash_info const&);

    void clear();

    void add_observer(weak_node<observer>);
    void remove_observer(weak_node<observer>);

    json::object check_integrity();

    // comparison
    bool operator==(trash_directory const&) const noexcept;
    std::strong_ordering operator<=>(trash_directory const&) const noexcept;

protected:
    trash_directory(string_view path, option);

private: // environment::directory_observer
    void file_created(string_view dirname, string_view filename) noexcept override;
    void file_modified(string_view dirname, string_view filename) noexcept override;
    void file_deleted(string_view dirname, string_view filename) noexcept override;
    void directory_disappeared(string_view dirname) noexcept override;

private:
    friend class impl;

    string m_path;
    entry_set m_entries;
    shared_node<class directory_size_cache> m_dir_sizes;
    shared_node<directory_settings> m_settings;

    observer_set<observer> m_observers;
};

void tag_invoke(json::value_from_tag, json::value&, trash_directory const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TRASH_DIRECTORY_HPP
