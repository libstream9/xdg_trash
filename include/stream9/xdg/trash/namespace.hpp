#ifndef STREAM9_TRASH_NAMESPACE_HPP
#define STREAM9_TRASH_NAMESPACE_HPP

namespace stream9::filesystem {}
namespace stream9::json {}
namespace stream9::strings {}
namespace stream9::ranges {}
namespace stream9::errors {}
namespace stream9::iterators {}
namespace stream9::linux {}

namespace stream9::xdg::trash {

namespace st9 = stream9;

namespace fs { using namespace stream9::filesystem; }
namespace json { using namespace stream9::json; }
namespace str { using namespace stream9::strings; }

namespace rng { using namespace stream9::ranges; }

namespace err { using namespace stream9::errors; }
namespace iter { using namespace stream9::iterators; }
namespace lx { using namespace stream9::linux; }

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_NAMESPACE_HPP
