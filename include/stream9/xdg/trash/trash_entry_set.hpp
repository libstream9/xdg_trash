#ifndef STREAM9_TRASH_TRASH_ENTRY_SET_HPP
#define STREAM9_TRASH_TRASH_ENTRY_SET_HPP

#include "namespace.hpp"
#include "trash_entry.hpp"

#include <stream9/less.hpp>
#include <stream9/projection.hpp>
#include <stream9/string.hpp>
#include <stream9/unique_array.hpp>

namespace stream9::xdg::trash {

class trash_directory;
class trash_info;

/*
 * @model std::semiregular
 * @model std::ranges::random_access_range
 */
class trash_entry_set
{
public:
    using set_type = unique_array<trash_entry, less, project<&trash_entry::name>>;
    using iterator = rng::iterator_t<set_type>;
    using const_iterator = rng::iterator_t<set_type const>;

public:
    // essential
    trash_entry_set() = default;

    // accessor
    auto begin() const noexcept { return m_entries.begin(); }
    auto end() const noexcept { return m_entries.end(); }

    auto begin() noexcept { return m_entries.begin(); }
    auto end() noexcept { return m_entries.end(); }

    // query
    auto size() const noexcept { return m_entries.size(); }

    const_iterator find(string_view filename) const noexcept;
    const_iterator find(trash_directory const&, trash_info const&) const noexcept;

    // modifier
    iterator insert(trash_info&, trash_directory&);
    void     insert(trash_directory&);

    iterator erase(trash_info const&, trash_directory&) noexcept;
    void     erase(trash_directory&) noexcept;
    iterator erase(const_iterator) noexcept;
    iterator erase(const_iterator, const_iterator) noexcept;

private:
    set_type m_entries;
};

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TRASH_ENTRY_SET_HPP
