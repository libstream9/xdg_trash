#ifndef STREAM9_TRASH_DIRECTORY_SETTINGS_HPP
#define STREAM9_TRASH_DIRECTORY_SETTINGS_HPP

#include "environment.hpp"
#include "file_size.hpp"
#include "namespace.hpp"

#include <stream9/cstring_view.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::trash {

/*
 * @model !std::copyable
 * @model !std::movable
 */
class directory_settings : public environment::directory_observer
{
public:
    // essentials
    static shared_node<directory_settings>
        construct(string_view dir_path);

    directory_settings(directory_settings const&) = delete;
    directory_settings& operator=(directory_settings const&) = delete;

    directory_settings(directory_settings&&) = delete;
    directory_settings& operator=(directory_settings&&) = delete;

    // accessor
    cstring_view path() const noexcept { return m_path; }

    // query
    file_size_t const&
        size_limit() const noexcept { return m_size_limit; }

    // modifier
    //void set_size_limit(file_size_t) noexcept;

protected:
    directory_settings(string_view dir_path);

private:
    // environment::directory_observer
    void file_created(string_view dirname, string_view /*filename*/) noexcept override;
    void file_modified(string_view dirname, string_view /*filename*/) noexcept override;
    void file_deleted(string_view dirname, string_view /*filename*/) noexcept override;

private:
    friend struct impl;

    string m_path;
    file_size_t m_size_limit;
};

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_DIRECTORY_SETTINGS_HPP
