#ifndef STREAM9_TRASH_TOP_DIRECTORY_HPP
#define STREAM9_TRASH_TOP_DIRECTORY_HPP

#include "environment.hpp"
#include "json_fwd.hpp"
#include "namespace.hpp"
#include "observer_set.hpp"

#include <compare>
#include <cstddef>
#include <iterator>

#include <stream9/cstring_view.hpp>
#include <stream9/iterators.hpp>
#include <stream9/optional.hpp>
#include <stream9/ranges/range_facade.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>

namespace stream9::xdg::trash {

class trash_directory;

/*
 * @model !std::movable
 * @model !std::copyable
 * @model std::totally_ordered
 * @model std::ranges::forward_range
 */
class top_directory : public rng::range_facade<top_directory>
                    , public environment::directory_observer
{
public:
    class observer
    {
    public:
        virtual ~observer() = default;

        virtual void trash_directory_added(trash_directory&) {}
        virtual void trash_directory_about_to_be_removed(trash_directory&) {}
        virtual void directory_disappeared(top_directory&) {}
    };

    template<typename T, typename Ref> class iterator_common;

    using iterator = iterator_common<top_directory, trash_directory&>;
    using const_iterator = iterator_common<top_directory const, trash_directory const&>;
    using size_type = safe_integer<int64_t, 0>;

public:
    // essential
    static shared_node<top_directory>
        construct(string_view);

    top_directory(top_directory const&) = delete;
    top_directory& operator=(top_directory const&) = delete;

    top_directory(top_directory&&) = delete;
    top_directory& operator=(top_directory&&) = delete;

    // accessor
    cstring_view path() const noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    iterator begin() noexcept;
    iterator end() noexcept;

    // query
    size_type size() const noexcept;

    trash_directory* primary_trash_directory() const noexcept;

    bool contains(trash_directory const&) const noexcept;

    // modifier
    trash_directory* find_or_create_trash_directory();

    void add_observer(weak_node<observer>);
    void remove_observer(weak_node<observer>);

    json::object check_integrity();

    // comparison
    bool operator==(top_directory const&) const noexcept;
    std::strong_ordering operator<=>(top_directory const&) const noexcept;

protected:
    top_directory(string_view);

private: // environment::directory_observer
    void file_created(string_view dirname, string_view /*filename*/) noexcept override;
    void file_deleted(string_view dirname, string_view /*filename*/) noexcept override;
    void directory_disappeared(string_view dirname) noexcept override;

private:
    friend struct impl;

    string m_path;

    opt<shared_node<trash_directory>> m_type1_trash_dir;
    opt<shared_node<trash_directory>> m_type2_trash_dir;

    observer_set<observer> m_observers;
};

template<typename T, typename Ref>
class top_directory::iterator_common : public iter::iterator_facade<
                                            iterator_common<T, Ref>,
                                            std::forward_iterator_tag,
                                            Ref >
{
    enum class position { type1, type2, end };

public:
    // essential
    iterator_common() = default;

    iterator_common(T& top_dir) noexcept;

private:
    friend class iter::iterator_core_access;

    Ref dereference() const noexcept;
    void increment() noexcept;
    bool equal(iterator_common const&) const noexcept;

private:
    T* m_top_dir {};
    position m_pos = position::end;
};

extern template class top_directory::iterator_common<top_directory, trash_directory&>;
extern template class top_directory::iterator_common<top_directory const, trash_directory const&>;

void tag_invoke(json::value_from_tag, json::value&, top_directory const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TOP_DIRECTORY_HPP
