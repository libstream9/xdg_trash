#ifndef STREAM9_TRASH_TRASH_CAN_HPP
#define STREAM9_TRASH_TRASH_CAN_HPP

#include "environment.hpp"
#include "json_fwd.hpp"
#include "top_directory_set.hpp"
#include "trash_directory.hpp"
#include "trash_entry.hpp"
#include "trash_entry_set.hpp"

#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/ranges/range_facade.hpp>

namespace stream9::xdg::trash {

class trash_info;

/*
 * @model !std::copyable
 * @model !std::movable
 * @model std::ranges::random_access_range
 */
class trash_can : public rng::range_facade<trash_can>
                , public top_directory_set::observer
                , public top_directory::observer
                , public trash_directory::observer
                , public enable_shared_from_this<trash_can>
{
public:
    using iterator = trash_entry_set::iterator;
    using const_iterator = trash_entry_set::const_iterator;

    class observer
    {
    public:
        virtual ~observer() = default;

        virtual void entry_created(trash_directory&, trash_info&) {}
        virtual void entry_deleted(trash_directory&, trash_info&) {}
        virtual void entry_modified(trash_directory&, trash_info&) {}
    };

public:
    // essential
    static shared_node<trash_can>
        construct();

    static shared_node<trash_can>
        construct(shared_node<environment>);

    trash_can(trash_can const&) = delete;
    trash_can& operator=(trash_can const&) = delete;

    trash_can(trash_can&&) = delete;
    trash_can& operator=(trash_can&&) = delete;

    // accessor
    trash_directory const& home_trash_directory() const noexcept;
    trash_directory&       home_trash_directory() noexcept;

    top_directory_set const& top_directories() const noexcept;
    top_directory_set&       top_directories() noexcept;

    const_iterator begin() const noexcept; // iterator of trash_entry const&
    const_iterator end() const noexcept;

    iterator begin() noexcept; // iterator of trash_entry&
    iterator end() noexcept;

    // query
    auto size() const noexcept { return m_entries.size(); }

    const_iterator find_trash_entry(string_view filename) const noexcept;

    const_iterator find_trash_entry(trash_directory&, trash_info&) const noexcept;

    trash_directory* trash_directory_for_file(string_view pathname) const;

    // modifier
    iterator trash_file(string_view pathname);

    void restore_file(const_iterator);

    void erase_file(const_iterator);

    void clear();

    json::object check_integrity();

protected:
    trash_can() noexcept;

private:
    // top_directory_set::observer
    void top_directory_added(top_directory&) noexcept override;
    void top_directory_about_to_be_removed(top_directory&) noexcept override;

    // top_directory::observer
    void trash_directory_added(trash_directory&) noexcept override;
    void trash_directory_about_to_be_removed(trash_directory&) noexcept override;

    // trash_directory::observer
    void trash_info_created(trash_directory&, trash_info&) override;
    void trash_info_deleted(trash_directory&, trash_info const&) override;
    void directory_disappeared(trash_directory&) override;

private:
    struct impl;

    shared_node<trash_directory> m_home_trash_dir;
    shared_node<top_directory_set> m_top_dirs;
    trash_entry_set m_entries;
    iterator m_last_inserted;
};

void tag_invoke(json::value_from_tag, json::value&, trash_can const&);

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_TRASH_CAN_HPP
