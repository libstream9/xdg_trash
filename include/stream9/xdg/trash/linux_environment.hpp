#ifndef STREAM9_TRASH_LINUX_ENVIRONMENT_HPP
#define STREAM9_TRASH_LINUX_ENVIRONMENT_HPP

#include "environment.hpp"
#include "observer_set.hpp"

#include <stream9/fstream.hpp>
#include <stream9/node.hpp>
#include <stream9/string.hpp>
#include <stream9/weak_node.hpp>

namespace stream9::xdg::trash {

class linux_environment : public environment
{
public:
    // essential
    linux_environment();
    ~linux_environment() noexcept;

    // query
    ::uid_t uid() noexcept override;

    string home_directory() override;

    string home_data_directory() override;

    array<string> mounted_top_directories() override;

    string top_directory(string_view dir_path) override;

    string absolute_path(string_view) override;

    string real_path(string_view) override;

    int monitor_fd() const noexcept override;

    bool exists(string_view pathname) override;

    bool is_directory(string_view pathname) noexcept override;

    bool is_directory_with_sticky_bit(string_view pathname) override;

    void list_directory(string_view dir_path,
                        function_ref<bool(string_view)> filename_cb) override;

    file_time_t current_time() override;

    file_time_t last_write_time(string_view pathname) override;

    file_size_t file_or_directory_size(cstring_ptr const&) override;

    file_size_t filesystem_capacity(string_view pathname) override;

    // modifier
    void move_file(string_view from_path, string_view to_path) override;

    void remove_file(string_view pathname) override;

    void remove_file_recursively(string_view pathname) override;

    void create_directory(string_view pathname) override;

    void add_mount_observer(weak_node<mount_observer>) override;

    void add_directory_observer(string_view dir_path,
                                weak_node<directory_observer>) override;
    // command
    ofstream create_file(string_view pathname) override;

    string load_file(string_view pathname, file_size_t size_limit) override;

    ofstream make_temporary_file(string_view prefix) override;

    void process_events(milliseconds /*timeout*/ = milliseconds(-1)) override;

private:
    struct impl;
    node<impl> m_impl;
};

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_LINUX_ENVIRONMENT_HPP
