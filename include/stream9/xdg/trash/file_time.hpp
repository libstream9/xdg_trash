#ifndef STREAM9_TRASH_FILE_TIME_HPP
#define STREAM9_TRASH_FILE_TIME_HPP

#include <chrono>

namespace stream9::xdg::trash {

using clock_t = std::chrono::system_clock;

using std::chrono::duration;

using std::chrono::seconds;
using std::chrono::milliseconds;
using std::chrono::nanoseconds;

using std::chrono::duration_cast;

template<typename Duration = clock_t::duration>
using file_time = std::chrono::time_point<clock_t, Duration>;

using file_time_t = file_time<nanoseconds>;

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_FILE_TIME_HPP
