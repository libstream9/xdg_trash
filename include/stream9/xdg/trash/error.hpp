#ifndef STREAM9_TRASH_ERROR_HPP
#define STREAM9_TRASH_ERROR_HPP

#include "namespace.hpp"

#include <source_location>
#include <system_error>

#include <stream9/errors.hpp>

namespace stream9::xdg::trash {

enum class errc {
    file_does_not_exist,
    file_already_exist,
    directory_does_not_exist,
    trash_directory_does_not_exist,
    file_is_too_big,
    fail_to_access_file,
    fail_to_get_file_size,
    fail_to_move_file,
    fail_to_delete_file,
    fail_to_create_trash_directory,
    fail_to_scan_trash_directory,
    fail_to_open_space_for_new_trash,
    fail_to_load_trash_info,
    fail_to_create_trash_info,
    fail_to_insert_trash_info,
    fail_to_delete_trash_info,
    fail_to_update_directory_size_cache,
    fail_to_create_top_directory,
    fail_to_find_top_directory,
    fail_to_save_directory_size_cache,
    fail_to_load_directory_settings,
    fail_to_monitor_directory,
    fail_to_monitor_mount,
    invalid_file_extension,
    parse_error,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

std::ostream& operator<<(std::ostream&, errc);

} // namespace stream9::xdg::trash

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::trash::errc> : true_type {};

} // namespace std

#endif // STREAM9_TRASH_ERROR_HPP
