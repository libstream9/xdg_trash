#ifndef STREAM9_TRASH_JSON_FWD_HPP
#define STREAM9_TRASH_JSON_FWD_HPP

namespace stream9::json {

struct value_from_tag;
class value;
class object;
class array;
class string;

} // namespace stream9::json

#endif // STREAM9_TRASH_JSON_FWD_HPP
