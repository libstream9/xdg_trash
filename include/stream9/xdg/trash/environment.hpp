#ifndef STREAM9_TRASH_ENVIRONMENT_HPP
#define STREAM9_TRASH_ENVIRONMENT_HPP

#include "file_size.hpp"
#include "file_time.hpp"
#include "namespace.hpp"

#include <string_view>
#include <system_error>

#include <unistd.h>

#include <stream9/array.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/function_ref.hpp>
#include <stream9/fstream.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/string.hpp>
#include <stream9/weak_node.hpp>

namespace stream9::xdg::trash {

class environment
{
public:
    class mount_observer
    {
    public:
        virtual ~mount_observer() = default;

        virtual void filesystem_mounted(string_view /*dir_path*/) {}
        virtual void filesystem_unmounted(string_view /*dir_path*/) {}
    };

    class directory_observer
    {
    public:
        virtual ~directory_observer() = default;

        virtual void file_created(string_view/*dir*/, string_view /*filename*/) {}
        virtual void file_modified(string_view/*dir*/, string_view /*filename*/) {}
        virtual void file_deleted(string_view/*dir*/, string_view /*filename*/) {}
        virtual void directory_disappeared(string_view/*dir*/) {}
    };

public:
    // essential
    virtual ~environment() = default;

    // query
    virtual uid_t uid() = 0;

    virtual string home_directory() = 0;

    virtual string home_data_directory() = 0;

    virtual array<string> mounted_top_directories() = 0;

    virtual string top_directory(string_view dir_path) = 0;

    virtual string absolute_path(string_view) = 0;

    virtual string real_path(string_view) = 0;

    virtual int monitor_fd() const noexcept = 0;

    virtual bool exists(string_view pathname) = 0;

    virtual bool is_directory(string_view pathname)  = 0;

    virtual bool is_directory_with_sticky_bit(string_view pathname) = 0;

    virtual void list_directory(string_view pathname,
                                function_ref<bool(string_view)> filename_cb) = 0;

    virtual file_time_t current_time() = 0;

    virtual file_time_t last_write_time(string_view pathname) = 0;

    virtual file_size_t file_or_directory_size(cstring_ptr const&) = 0;

    virtual file_size_t filesystem_capacity(string_view pathname) = 0;

    virtual string load_file(string_view pathname, file_size_t size_limit) = 0;

    // modifier
    virtual void move_file(string_view from_path, string_view to_path) = 0;

    virtual void remove_file(string_view pathname) = 0;

    virtual void remove_file_recursively(string_view pathname) = 0;

    virtual void create_directory(string_view pathname) = 0;

    virtual void add_mount_observer(weak_node<mount_observer>) = 0;

    virtual void add_directory_observer(string_view dir_path,
                                        weak_node<directory_observer>) = 0;

    // command
    virtual ofstream create_file(string_view pathname) = 0;

    virtual void process_events(milliseconds /*timeout*/ = milliseconds(-1)) = 0;

    virtual ofstream make_temporary_file(string_view prefix) = 0;
};

environment& env();

void set_env(shared_node<environment>) noexcept;

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_ENVIRONMENT_HPP
