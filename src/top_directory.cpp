#include <stream9/xdg/trash/top_directory.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/environment.hpp>
#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <cassert>

#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/append.hpp> // operator/=
#include <stream9/path/concat.hpp> // operator/
#include <stream9/to_string.hpp>

namespace stream9::xdg::trash {

using path::operator/;

static bool
directory_exists(string_view p)
{
    try {
        return env().is_directory(p);
    }
    catch (...) {
        throw_error(errc::fail_to_access_file, {
            { "path", p }
        });
    }
}

static opt<string>
type1_trash_directory(string_view top, string_view uid) noexcept
{
    using path::operator/=;

    opt<string> result;

    try {
        auto path = top / ".Trash";

        if (!env().is_directory_with_sticky_bit(path)) return result;

        path /= uid;

        result = std::move(path);
    }
    catch (...) {
        // nop
    }

    return result;
}

static string
type2_trash_directory(string_view top, string_view uid)
{
    try {
        auto path = top / ".Trash-";
        path += uid;

        return path;
    }
    catch (...) {
        rethrow_error();
    }
}

static opt<shared_node<trash_directory>>
create_type1_trash_directory(string_view top, string_view uid)
{
    try {
        opt<shared_node<trash_directory>> result;

        if (auto o_dir = type1_trash_directory(top, uid)) {
            try {
                result = trash_directory::construct(
                    std::move(*o_dir),
                    trash_directory::option { .create_if_not_exist = true }
                );
            }
            catch (...) {
                throw_error(errc::fail_to_create_trash_directory, {
                    { "path", *o_dir },
                });
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "top directory", top },
            { "uid", uid }
        });
    }
}

static opt<shared_node<trash_directory>>
create_type2_trash_directory(string_view top, string_view uid) noexcept
{
    try {
        opt<shared_node<trash_directory>> result;

        auto const& path = type2_trash_directory(top, uid);
        try {
            result = trash_directory::construct(
                path,
                trash_directory::option { .create_if_not_exist = true }
            );
        }
        catch (...) {
            throw_error(errc::fail_to_create_trash_directory, {
                { "path", path }
            });
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "top directory", top },
            { "uid", uid }
        });
    }
}

static opt<shared_node<trash_directory>>
load_trash_directory(string_view p) noexcept
{
    try {
        return trash_directory::construct(std::move(p));
    }
    catch (error const& e) {
        if (e.why() == errc::directory_does_not_exist) {
            // ignore
        }
        else {
            print_error(log::warn());
        }
    }
    return {};
}

static bool
is_directory_noexcept(string_view p) noexcept
{
    try {
        return env().is_directory(p);
    }
    catch (...) {
        return false;
    }
}

struct impl
{
    top_directory* m_p; // non-null

    // essential
    impl(top_directory& d) : m_p { &d } {}

    void scan_trash_directories()
    {
        try {
            auto uid = to_string(env().uid());

            auto o_path1 = type1_trash_directory(m_p->m_path, uid);
            if (o_path1 && is_directory_noexcept(*o_path1)) {
                if (!m_p->m_type1_trash_dir) {
                    m_p->m_type1_trash_dir =
                        load_trash_directory(std::move(*o_path1));

                    m_p->m_observers.for_each([&](auto&& o) {
                        o.trash_directory_added(*m_p->m_type1_trash_dir);
                    });
                }
            }
            else {
                if (m_p->m_type1_trash_dir) {
                    m_p->m_observers.for_each([&](auto&& o) {
                        o.trash_directory_about_to_be_removed(*m_p->m_type1_trash_dir);
                    });

                    m_p->m_type1_trash_dir.reset();
                }
            }

            auto path2 = type2_trash_directory(m_p->m_path, uid);
            if (is_directory_noexcept(path2)) {
                if (!m_p->m_type2_trash_dir) {
                    m_p->m_type2_trash_dir =
                        load_trash_directory(std::move(path2));

                    m_p->m_observers.for_each([&](auto&& o) {
                        o.trash_directory_added(*m_p->m_type2_trash_dir);
                    });
                }
            }
            else {
                if (m_p->m_type2_trash_dir) {
                    m_p->m_observers.for_each([&](auto&& o) {
                        o.trash_directory_about_to_be_removed(*m_p->m_type2_trash_dir);
                    });

                    m_p->m_type2_trash_dir.reset();
                }
            }
        }
        catch (...) {
            throw_error(errc::fail_to_scan_trash_directory, {
                { "path", m_p->m_path }
            });
        }
    }
};

/*
 * top_directory
 */
static_assert(!std::movable<top_directory>);
static_assert(!std::copyable<top_directory>);
static_assert(std::totally_ordered<top_directory>);
static_assert(std::ranges::forward_range<top_directory>);

struct top_directory_impl : top_directory
{
    top_directory_impl(string_view p)
        : top_directory { p }
    {}
};

shared_node<top_directory> top_directory::
construct(string_view p)
{
    try {
        shared_node<top_directory_impl> result { p };

        try {
            env().add_directory_observer(result->path(), result);
        }
        catch (...) {
            throw_error(errc::fail_to_monitor_directory);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

top_directory::
top_directory(string_view path)
    : m_path { path }
{
    try {
        if (!path::is_absolute(m_path)) {
            m_path = trash::env().absolute_path(m_path);
        }

        if (!directory_exists(m_path)) {
            throw_error(errc::directory_does_not_exist);
        }

        impl(*this).scan_trash_directories();
    }
    catch (...) {
        rethrow_error({ { "path", m_path } });
    }
}

cstring_view top_directory::
path() const noexcept
{
    return m_path;
}

top_directory::const_iterator top_directory::
begin() const noexcept
{
    return { *this };
}

top_directory::const_iterator top_directory::
end() const noexcept
{
    return {};
}

top_directory::iterator top_directory::
begin() noexcept
{
    return { *this };
}

top_directory::iterator top_directory::
end() noexcept
{
    return {};
}

top_directory::size_type top_directory::
size() const noexcept
{
    if (m_type1_trash_dir) {
        return m_type2_trash_dir ? 2 : 1;
    }
    else {
        return m_type2_trash_dir ? 1 : 0;
    }
}

trash_directory* top_directory::
primary_trash_directory() const noexcept
{
    if (m_type1_trash_dir) {
        return &*m_type1_trash_dir;
    }
    else if (m_type2_trash_dir) {
        return &*m_type2_trash_dir;
    }
    else {
        return nullptr;
    }
}

bool top_directory::
contains(trash_directory const& d) const noexcept
{
    return (m_type1_trash_dir && &*m_type1_trash_dir == &d)
        || (m_type2_trash_dir && &*m_type2_trash_dir == &d);
}

trash_directory* top_directory::
find_or_create_trash_directory()
{
    try {
        auto uid = to_string(env().uid());

        if (m_type1_trash_dir) {
            return &*m_type1_trash_dir;
        }
        else {
            m_type1_trash_dir = create_type1_trash_directory(m_path, uid);
            if (m_type1_trash_dir) {
                m_observers.for_each([&](auto& ob) {
                    ob.trash_directory_added(*m_type1_trash_dir);
                });

                return &*m_type1_trash_dir;
            }
        }

        if (m_type2_trash_dir) {
            return &*m_type2_trash_dir;
        }
        else {
            m_type2_trash_dir = create_type2_trash_directory(m_path, uid);
            if (m_type2_trash_dir) {
                m_observers.for_each([&](auto& ob) {
                    ob.trash_directory_added(*m_type2_trash_dir);
                });

                return &*m_type2_trash_dir;
            }
        }

        return nullptr;
    }
    catch (...) {
        rethrow_error();
    }
}

void top_directory::
add_observer(weak_node<observer> const o)
{
    try {
        m_observers.insert(o);
    }
    catch (...) {
        rethrow_error();
    }
}

void top_directory::
remove_observer(weak_node<observer> const o)
{
    try {
        m_observers.erase(o);
    }
    catch (...) {
        rethrow_error();
    }
}

json::object top_directory::
check_integrity()
{
    try {
        json::object rv;
        json::array dirs;

        if (m_type1_trash_dir) {
            auto e = m_type1_trash_dir->check_integrity();
            if (!e.empty()) {
                dirs.push_back(std::move(e));
            }
        }
        if (m_type2_trash_dir) {
            auto e = m_type2_trash_dir->check_integrity();
            if (!e.empty()) {
                dirs.push_back(std::move(e));
            }
        }

        if (!dirs.empty()) {
            rv["path"] = m_path;
            rv["trash directories"] = std::move(dirs);
        }

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

bool top_directory::
operator==(top_directory const& other) const noexcept
{
    return m_path == other.m_path;
}

std::strong_ordering top_directory::
operator<=>(top_directory const& other) const noexcept
{
    return m_path <=> other.m_path;
}

// environment::directory_observer
void top_directory::
file_created(string_view, string_view /*filename*/) noexcept
{
    try {
        impl(*this).scan_trash_directories();
    }
    catch (...) {
        print_error(log::warn());
    }
}

void top_directory::
file_deleted(string_view, string_view /*filename*/) noexcept
{
    try {
        impl(*this).scan_trash_directories();
    }
    catch (...) {
        print_error(log::warn());
    }
}

void top_directory::
directory_disappeared(string_view) noexcept
{
    try {
        m_observers.for_each(
            [&](auto&& o) {
                o.directory_disappeared(*this);
            });
    }
    catch (...) {
        print_error(log::warn());
    }
}

/*
 * class top_directory::iterator_common
 */
template<typename T, typename R>
top_directory::iterator_common<T, R>::
iterator_common(T& top_dir) noexcept
    : m_top_dir { &top_dir }
{
    if (m_top_dir->m_type1_trash_dir) {
        m_pos = position::type1;
    }
    else if (m_top_dir->m_type2_trash_dir) {
        m_pos = position::type2;
    }
    else {
        m_pos = position::end;
    }
}

template<typename T, typename R>
R top_directory::iterator_common<T, R>::
dereference() const noexcept
{
    if (m_pos == position::type1) {
        return *m_top_dir->m_type1_trash_dir;
    }
    else {
        assert(m_pos != position::end);
        return *m_top_dir->m_type2_trash_dir;
    }
}

template<typename T, typename R>
void top_directory::iterator_common<T, R>::
increment() noexcept
{
    if (m_pos == position::type1) {
        if (m_top_dir->m_type2_trash_dir) {
            m_pos = position::type2;
        }
        else {
            m_pos = position::end;
        }
    }
    else if (m_pos == position::type2) {
        m_pos = position::end;
    }
}

template<typename T, typename R>
bool top_directory::iterator_common<T, R>::
equal(iterator_common const& other) const noexcept
{
    return m_pos == other.m_pos;
}

// explicit instantiation
template class top_directory::iterator_common<top_directory, trash_directory&>;
template class top_directory::iterator_common<top_directory const, trash_directory const&>;

void
tag_invoke(json::value_from_tag, json::value& v, top_directory const& d)
{
    auto& obj = v.emplace_object();
    obj["path"] = d.path();

    json::array trash_dirs;
    for (auto const& td: d) {
        trash_dirs.push_back(td);
    }

    obj["trash_dirs"] = std::move(trash_dirs);
}

} // namespace stream9::xdg::trash
