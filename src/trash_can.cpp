#include <stream9/xdg/trash/trash_can.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/linux_environment.hpp>
#include <stream9/xdg/trash/top_directory.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <stream9/container.hpp>
#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp> // operator/

namespace stream9::xdg::trash {

using path::operator/;

static string
home_trash_path()
{
    try {
        return env().home_data_directory() / "Trash";
    }
    catch (...) {
        rethrow_error();
    }
}

static shared_node<trash_directory>
make_home_trash_directory()
{
    try {
        return trash_directory::construct(
            home_trash_path(), {
                .create_if_not_exist = true
            });
    }
    catch (...) {
        throw_error(errc::fail_to_create_trash_directory);
    }
}

/*
 * class trash_can
 */
static_assert(!std::copyable<trash_can>);
static_assert(!std::movable<trash_can>);
static_assert(std::ranges::random_access_range<trash_can>);

struct trash_can_impl : trash_can
{
    trash_can_impl() = default;
};

shared_node<trash_can> trash_can::
construct()
{
    return construct(
        shared_node<linux_environment>()
    );
}

shared_node<trash_can> trash_can::
construct(shared_node<environment> env)
{
    try {
        set_env(env);

        shared_node<trash_can> result =
            shared_node<trash_can_impl>();

        result->m_home_trash_dir->add_observer(result);

        result->m_top_dirs->add_observer(result);

        for (auto& top_dir: *result->m_top_dirs) {
            top_dir.add_observer(result);

            for (auto& trash_dir: top_dir) {
                trash_dir.add_observer(result);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

trash_can::
trash_can() noexcept
    : m_home_trash_dir { make_home_trash_directory() }
    , m_top_dirs { top_directory_set::construct() }
{
    try {
        m_entries.insert(*m_home_trash_dir);
    }
    catch (...) {
        print_error(log::warn());
    }

    for (auto& top_dir: *m_top_dirs) {
        for (auto& trash_dir: top_dir) {
            try {
                m_entries.insert(trash_dir);
            }
            catch (...) {
                print_error(log::warn());
            }
        }
    }
}

trash_directory const& trash_can::
home_trash_directory() const noexcept
{
    return *m_home_trash_dir;
}

trash_directory& trash_can::
home_trash_directory() noexcept
{
    return *m_home_trash_dir;
}

top_directory_set const& trash_can::
top_directories() const noexcept
{
    return *m_top_dirs;
}

top_directory_set& trash_can::
top_directories() noexcept
{
    return *m_top_dirs;
}

trash_can::const_iterator trash_can::
begin() const noexcept
{
    return m_entries.begin();
}

trash_can::const_iterator trash_can::
end() const noexcept
{
    return m_entries.end();
}

trash_can::iterator trash_can::
begin() noexcept
{
    return m_entries.begin();
}

trash_can::iterator trash_can::
end() noexcept
{
    return m_entries.end();
}

trash_can::const_iterator trash_can::
find_trash_entry(string_view filename) const noexcept
{
    return m_entries.find(filename);
}

trash_can::const_iterator trash_can::
find_trash_entry(trash_directory& d, trash_info& i) const noexcept
{
    return m_entries.find(d, i);
}

trash_directory* trash_can::
trash_directory_for_file(string_view file_path) const
{
    try {
        auto const& home_top_dir_path = env().top_directory(env().home_directory());
        auto const& trash_top_dir_path = env().top_directory(file_path);

        if (home_top_dir_path == trash_top_dir_path) {
            return &m_home_trash_dir;
        }
        else {
            auto const it = m_top_dirs->find(trash_top_dir_path);
            if (it != m_top_dirs->end()) {
                return it->find_or_create_trash_directory();
            }
        }

        return nullptr;
    }
    catch (...) {
        rethrow_error({ { "file path", file_path } });
    }
}

trash_can::iterator trash_can::
trash_file(string_view trash_path)
{
    try {
        if (!env().exists(trash_path)) {
            throw_error(errc::file_does_not_exist);
        }

        auto canonical_path = env().real_path(trash_path);

        auto* trash_dir = trash_directory_for_file(canonical_path);

        if (!trash_dir) {
            trash_dir = &m_home_trash_dir;
        }

        trash_dir->trash_file(canonical_path);

        return m_last_inserted;
    }
    catch (...) {
        rethrow_error({ { "trash_path", trash_path } });
    }
}

void trash_can::
restore_file(const_iterator it)
{
    try {
        if (it == m_entries.end()) return;

        auto& dir = it->trash_directory();
        auto& info = it->trash_info();

        dir.restore_trash(info);
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_can::
erase_file(const_iterator it)
{
    try {
        if (it == m_entries.end()) return;

        auto& dir = it->trash_directory();
        auto& info = it->trash_info();

        dir.delete_trash(info);
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_can::
clear()
{
    try {
        m_home_trash_dir->clear();

        for (auto& top_dir: *m_top_dirs) {
            for (auto& trash_dir: top_dir) {
                trash_dir.clear();
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
is_valid_trash_directory(auto& top_dirs, trash_directory const& dir) noexcept
{
    for (auto const& top: top_dirs) {
        for (auto const& d: top) {
            if (&dir == &d) return true;;
        }
    }
    return false;
}

json::object trash_can::
check_integrity()
{
    try {
        json::object rv;

        auto home_trash_dir = m_home_trash_dir->check_integrity();
        if (!home_trash_dir.empty()) {
            rv["home trash directory"] = std::move(home_trash_dir);
        }

        json::array top_dirs;
        for (auto& td: *m_top_dirs) {
            auto top_dir = td.check_integrity();
            if (!top_dir.empty()) {
                top_dirs.push_back(std::move(top_dir));
            }
        }
        if (!top_dirs.empty()) {
            rv["top directories"] = std::move(top_dirs);
        }

        json::array ent_errors;
        for (auto const& ent: m_entries) {
            auto const& dir = ent.trash_directory();

            if (&dir != &m_home_trash_dir &&
                !is_valid_trash_directory(*m_top_dirs, dir))
            {
                ent_errors.push_back(json::object {
                    { "description", "dangling trash directory" },
                    { "directory path", dir.path() },
                });
                continue;
            }

            auto const& info = ent.trash_info();
            bool invalid = std::none_of(
                dir.begin(), dir.end(),
                [&](auto&& i) {
                    return &i == &info;
                }
            );
            if (invalid) {
                ent_errors.push_back(json::object {
                    { "description", "dangling trash info" },
                    { "trash info path", info.info_path() }
                });
            }
        }
        if (!ent_errors.empty()) {
            rv["entries"] = std::move(ent_errors);
        }

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_can::
top_directory_added(top_directory& top_dir) noexcept
{
    try {
        top_dir.add_observer(this->weak_from_this());

        for (auto& trash_dir: top_dir) {
            m_entries.insert(trash_dir);
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
top_directory_about_to_be_removed(top_directory& top_dir) noexcept
{
    try {
        for (auto& trash_dir: top_dir) {
            m_entries.erase(trash_dir);
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
trash_directory_added(trash_directory& d) noexcept
{
    try {
        d.add_observer(this->weak_from_this());

        m_entries.insert(d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
trash_directory_about_to_be_removed(trash_directory& d) noexcept
{
    try {
        m_entries.erase(d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
trash_info_created(trash_directory& d, trash_info& i)
{
    try {
        m_last_inserted = m_entries.insert(i, d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
trash_info_deleted(trash_directory& d, trash_info const& i)
{
    try {
        m_entries.erase(i, d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_can::
directory_disappeared(trash_directory& d)
{
    try {
        m_entries.erase(d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void
tag_invoke(json::value_from_tag, json::value& v, trash_can const& can)
{
    auto& obj = v.emplace_object();

    obj["home_trash"] = can.home_trash_directory();
    obj["top_dirs"] = can.top_directories();

    json::object entries;
    for (auto const& e: can) {
        entries[e.name()] = e.trash_info().info_path();
    }

    obj["entries"] = std::move(entries);
}

} // namespace stream9::xdg::trash
