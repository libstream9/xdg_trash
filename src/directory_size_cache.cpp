#include <stream9/xdg/trash/directory_size_cache.hpp>

#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <regex>
#include <utility>

#include <stream9/from_string.hpp>
#include <stream9/fstream.hpp>
#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/strings/url_decode.hpp>
#include <stream9/strings/url_encode.hpp>
#include <stream9/to_string.hpp>

namespace stream9::xdg::trash {

using path::operator/;

static constexpr string_view
cache_filename() noexcept
{
    return "directorysizes";
}

static string
decode_filename(string_view s)
{
    try {
        return str::url_decode(s);
    }
    catch (...) {
        rethrow_error();
    }
}

static string
encode_filename(string_view s)
{
    try {
        return str::url_encode(s);
    }
    catch (...) {
        rethrow_error();
    }
}

using mtime_t = directory_size_cache::entry::mtime_t;

static mtime_t
decode_mtime(std::int64_t t) noexcept
{
    // trash specification states tick is seconds from unix epoch but KIO store milliseconds
    duration<std::int64_t, std::milli> msecs_since_epoch { t };

    return mtime_t { msecs_since_epoch };
}

static int64_t
encode_mtime(mtime_t t) noexcept
{
    duration<std::int64_t, std::milli> msecs { t.time_since_epoch() };
    return msecs.count();
}

static mtime_t
to_mtime(file_time_t t)
{
    return mtime_t {
        duration_cast<milliseconds>(t.time_since_epoch())
    };
}

static string_view
to_string_view(auto const& m) noexcept
{
    return { &*m.first, &*m.second };
}

static opt<directory_size_cache::entry>
parse_line(string_view line, string_view path)
{
    try {
        opt<directory_size_cache::entry> result;

        static std::regex pattern {
            R"(\s*)"            // 0 or more space
            R"(([[:digit:]]+))" // size
            R"(\s+)"            // 1 or more space
            R"(([[:digit:]]+))" // time
            R"(\s+)"            // 1 or more space
            R"((.+))"           // filename
            R"(\s*)"            // 0 or more space
        };

        std::cmatch match;
        if (!std::regex_match(line.begin(), line.end(), match, pattern)) {
            log::warn() << "invalid line on directory size cache: " << path;
            return result;
        }

        auto size = str::from_string<std::int64_t>(to_string_view(match[1]));
        auto time = str::from_string<std::int64_t>(to_string_view(match[2]));
        auto filename = to_string_view(match[3]);

        result.emplace(
            decode_filename(filename),
            static_cast<file_size_t>(size),
            decode_mtime(time)
        );

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
load_cache(auto& entries, auto& time_stamp, string_view p)
{
    try {
        cstring_ptr path { p };

        time_stamp = env().last_write_time(path);

        st9::ifstream ifs { path };

        std::string line;
        while (true) {
            std::getline(ifs, line);
            if (ifs.eof()) break;
            if (ifs.fail()) return false;

            if (line.empty()) continue;

            if (auto o_entry = parse_line(line, path)) {
                entries.insert(std::move(*o_entry));
            }
        }

        return true;
    }
    catch (...) {
        return false;
    }
}

static void
scan_trash_files(auto& entries, auto const& trash_dir)
{
    try {
        for (auto const& info: trash_dir) {
            if (!env().is_directory(info.file_path())) continue;

            auto const& file_path = info.file_path();

            entries.insert({
                .filename = string(path::basename(file_path)),
                .size = env().file_or_directory_size(file_path),
                .mtime = to_mtime(info.time_stamp()),
            });
        }
    }
    catch (...) {
        throw_error(errc::fail_to_scan_trash_directory, {
            { "path", trash_dir.path() }
        });
    }
}

static void
save_cache(auto& entries, file_time_t& timestamp, string_view p)
{
    try {
        auto const& dir_path = path::dirname(p);
        if (!env().exists(dir_path)) return;

        auto ofs = env().make_temporary_file(dir_path / "cache_");
        ofs.exceptions(ofs.failbit);

        for (auto const& ent: entries) {
            ofs << ent.size
                << " " << encode_mtime(ent.mtime)
                << " " << encode_filename(ent.filename)
                << std::endl;
        }

        env().move_file(ofs.fd().path(), p);

        timestamp = env().last_write_time(p);
    }
    catch (...) {
        throw_error(errc::fail_to_save_directory_size_cache, {
            { "path", p }
        });
    }
}

/*
 * class directory_size_cache
 */
static_assert(!std::movable<directory_size_cache>);
static_assert(!std::copyable<directory_size_cache>);
static_assert(std::ranges::random_access_range<directory_size_cache>);

struct directory_size_cache_impl : directory_size_cache
{
    directory_size_cache_impl(trash_directory const& d)
        : directory_size_cache { d } {}
};

shared_node<directory_size_cache> directory_size_cache::
construct(trash_directory const& d)
{
    try {
        shared_node<directory_size_cache_impl> result { d };

        env().add_directory_observer(d.path(), result);

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

directory_size_cache::
directory_size_cache(trash_directory const& d)
    : m_trash_dir { d }
    , m_path { d.path() / cache_filename() }
{
    try {
        if (!load_cache(m_entries, m_time_stamp, m_path)) {
            scan_trash_files(m_entries, m_trash_dir);
            if (!m_entries.empty()) {
                save_cache(m_entries, m_time_stamp, m_path);
            }
        }
    }
    catch (...) {
        rethrow_error({ { "directory", d.path() } });
    }
}

directory_size_cache::const_iterator directory_size_cache::
begin() const noexcept
{
    return m_entries.begin();
}

directory_size_cache::const_iterator directory_size_cache::
end() const noexcept
{
    return m_entries.end();
}

file_time_t directory_size_cache::
last_modified_time() const noexcept
{
    return m_time_stamp;
}

directory_size_cache::size_type directory_size_cache::
size() const noexcept
{
    return m_entries.size();
}

directory_size_cache::const_iterator directory_size_cache::
find(string_view filename) const noexcept
{
    return m_entries.find(filename);
}

directory_size_cache::iterator directory_size_cache::
find(string_view filename) noexcept
{
    return m_entries.find(filename);
}

cstring_view directory_size_cache::
path() const noexcept
{
    return m_path;
}

bool directory_size_cache::
insert_or_update_entry(trash_info const& info)
{
    try {
        bool changed = false;
        auto const& path = info.file_path();

        if (!env().is_directory(path)) return changed;

        auto size = env().file_or_directory_size(path);
        auto mtime = to_mtime(info.time_stamp());

        auto [it, ok] =  m_entries.insert({
            .filename = string(path::basename(path)),
            .size = size,
            .mtime = mtime
        });
        if (ok) {
            changed = true;
        }
        else if (it != m_entries.end()) {
            if (it->size != size) {
                it->size = size;
                changed = true;
            }
            if (it->mtime != mtime) {
                it->mtime = mtime;
                changed = true;
            }
        }

        if (changed) {
            save_cache(m_entries, m_time_stamp, this->path());
        }

        return changed;
    }
    catch (...) {
        rethrow_error({
            { "path", path() },
            { "trash_info", info },
        });
    }
}

bool directory_size_cache::
update_or_insert_entry(trash_info const& info)
{
    try {
        bool changed = false;
        auto const& path = info.file_path();

        if (!env().is_directory(path)) return changed;

        auto size = env().file_or_directory_size(path);
        auto mtime = to_mtime(info.time_stamp());

        auto it = find(path::basename(path));
        if (it != m_entries.end()) {
            it->size = size;
            it->mtime = mtime;
            changed = true;
        }
        else {
            m_entries.insert({
                .filename = string(path::basename(path)),
                .size = size,
                .mtime = mtime
            });
            changed = true;
        }

        if (changed) {
            save_cache(m_entries, m_time_stamp, this->path());
        }

        return changed;
    }
    catch (...) {
        rethrow_error({
            { "path", path() },
            { "trash_info", info },
        });
    }
}

void directory_size_cache::
erase_entry(trash_info const& info)
{
    try {
        auto file_path = info.file_path();

        auto it = find(path::basename(file_path));
        if (it != m_entries.end()) {
            m_entries.erase(it);
            save_cache(m_entries, m_time_stamp, m_path);
        }
    }
    catch (...) {
        rethrow_error({
            { "path", path() },
            { "trash_info", info },
        });
    }
}

void directory_size_cache::
refresh()
{
    try {
        try {
            auto mtime = env().last_write_time(path());
            if (mtime == m_time_stamp) return;
        }
        catch (...) {
            // ignore error and proceed
        }

        m_entries.clear();

        if (!load_cache(m_entries, m_time_stamp, path())) {
            scan_trash_files(m_entries, m_trash_dir);
            if (!m_entries.empty()) {
                save_cache(m_entries, m_time_stamp, m_path);
            }
        }
    }
    catch (...) {
        rethrow_error({ { "path", path() } });
    }
}

void directory_size_cache::
clear() noexcept
{
    m_entries.clear();
    m_time_stamp = {};
}

json::object directory_size_cache::
check_integrity()
{
    json::object rv;
    json::array errors;

    try {
        entry_set_t entries;
        file_time_t ts;

        if (!load_cache(entries, ts, m_path)) {
            return  rv;
        }

        if (ts != m_time_stamp) {
            errors.push_back(json::object {
                { "message", "discrepancy in time stamp of cache" },
                { "memory", st9::to_string(m_time_stamp) },
                { "file", st9::to_string(ts) },
            });
            m_entries = std::move(entries);
            m_time_stamp = std::move(ts);
        }
        else if (entries != m_entries) {
            errors.push_back(json::object {
                { "message", "discrepancy between cache on memory and file." },
                { "memory", m_entries },
                { "file", entries },
            });
            m_entries = std::move(entries);
            m_time_stamp = std::move(ts);
        }

        bool need_to_save = false;
        auto it = m_entries.begin();
        while (it != m_entries.end()) {
            auto& ent = *it;
            auto const& p = m_trash_dir.path() / "files" / ent.filename;

            if (!env().exists(p)) {
                errors.push_back(json::object {
                    { "entry", ent.filename },
                    { "message", "There is a cache entry for file that doesn't exist." },
                });
                it = m_entries.erase(it);
                need_to_save = true;
            }
            else {
                ++it;
            }
        }

        if (need_to_save) {
            save_cache(m_entries, m_time_stamp, m_path);
        }
    }
    catch (...) {
        errors.push_back(json::object {
            { "error", json::value_from(std::current_exception()) },
        });
    }

    if (!errors.empty()) {
        rv["path"] = m_path;
        rv["errors"] = std::move(errors);
    }

    return rv;
}

// environment::directory_observer
void directory_size_cache::
file_created(string_view, string_view filename) noexcept
{
    try {
        if (filename == cache_filename()) {
            entry_set_t entries;
            file_time_t time_stamp;

            if (load_cache(entries, time_stamp, path())) {
                m_entries = std::move(entries);
                m_time_stamp = std::move(time_stamp);
            }
        }
    }
    catch (...) {
        print_error(log::warn(), {
            { "path", path() },
            { "filename", filename }
        });
    }
}

void directory_size_cache::
file_modified(string_view, string_view filename) noexcept
{
    try {
        if (filename == cache_filename()) {
            refresh();
        }
    }
    catch (...) {
        print_error(log::warn(), {
            { "path", path() },
            { "filename", filename }
        });
    }
}

void directory_size_cache::
file_deleted(string_view, string_view filename) noexcept
{
    try {
        if (filename == cache_filename()) {
            clear();
        }
    }
    catch (...) {
        print_error(log::warn(), {
            { "path", path() },
            { "filename", filename }
        });
    }
}

void
tag_invoke(json::value_from_tag, json::value& v,
           directory_size_cache::entry const& e)
{
    v = json::object {
        { "filename", e.filename },
        { "size", e.size },
        { "mtime", str::to_string(e.mtime) },
    };
}

void
tag_invoke(json::value_from_tag, json::value& v, directory_size_cache const& d)
{
    auto& arr = v.emplace_array();

    for (auto const& ent: d) {
        arr.push_back(ent);
    }
}

} // namespace stream9::xdg::trash
