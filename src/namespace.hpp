#ifndef STREAM9_TRASH_SRC_NAMESPACE_HPP
#define STREAM9_TRASH_SRC_NAMESPACE_HPP

#include <stream9/xdg/trash/namespace.hpp>

namespace stream9::container {}

namespace stream9::xdg::trash {

namespace con { using namespace stream9::container; }

} // namespace stream9::xdg::trash

#endif // STREAM9_TRASH_SRC_NAMESPACE_HPP
