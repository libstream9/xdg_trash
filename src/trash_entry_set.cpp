#include <stream9/xdg/trash/trash_entry_set.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/trash_directory.hpp>
#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/trash_info.hpp>

#include <algorithm>
#include <cassert>

#include <stream9/container.hpp>
#include <stream9/contains.hpp>
#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::xdg::trash {

static auto
make_unique_name(auto& entries, auto& info)
{
    try {
        using str::operator<<;

        string result { info.name() };
        int num = 1;

        while (contains(entries, result)) {
            result = info.name();
            result << " (" << num << ")";
            ++num;
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static auto
do_insert(auto& entries, trash_info& info, trash_directory& dir)
{
    try {
        auto it = entries.insert({
            make_unique_name(entries, info),
            dir,
            info
        });

        return it;
    }
    catch (...) {
        rethrow_error();
    }
}

static bool
is_trash_info_inserted(auto& entries, trash_info const& info) noexcept
{
    using std::ranges::any_of;

    return any_of(entries,
        [&](auto& e) { return e.trash_info().info_path() == info.info_path(); });
}

static bool
is_directory_inserted(auto& entries, trash_directory const& dir) noexcept
{
    using std::ranges::any_of;

    return any_of(entries,
        [&](auto& e) { return e.trash_directory() == dir; });
}

/*
 * class trash_entry_set
 */
static_assert(std::semiregular<trash_entry_set>);
static_assert(std::ranges::random_access_range<trash_entry_set>);

trash_entry_set::const_iterator trash_entry_set::
find(string_view filename) const noexcept
{
    return m_entries.find(filename);
}

trash_entry_set::const_iterator trash_entry_set::
find(trash_directory const& d, trash_info const& i) const noexcept
{
    using std::ranges::find_if;

    return find_if(m_entries, [&](auto&& e) {
        return &e.trash_directory() == &d
            && &e.trash_info() == &i;
    });
}

trash_entry_set::iterator trash_entry_set::
insert(trash_info& info, trash_directory& dir)
{
    try {
        if (is_trash_info_inserted(m_entries, info)) {
            return m_entries.end();
        }

        return do_insert(m_entries, info, dir);
    }
    catch (...) {
        rethrow_error({
            { "info", info },
            { "dir", dir }
        });
    }
}

void trash_entry_set::
insert(trash_directory& dir)
{
    try {
        if (is_directory_inserted(m_entries, dir)) {
            return;
        }

        for (auto& info: dir) {
            do_insert(m_entries, info, dir);
        }
    }
    catch (...) {
        rethrow_error({ { "dir", dir } });
    }
}

trash_entry_set::iterator trash_entry_set::
erase(trash_info const& info, trash_directory& dir) noexcept
{
    using std::ranges::find_if;

    auto it = find_if(m_entries,
        [&](auto&& e) {
            return e.trash_directory() == dir
                && e.trash_info().name() == info.name();
        });
    if (it != m_entries.end()) {
        m_entries.erase(it);
    }

    return it;
}

void trash_entry_set::
erase(trash_directory& d) noexcept
{
    st9::erase_if(m_entries,
        [&](auto&& e) { return e.trash_directory() == d; });
}

trash_entry_set::iterator trash_entry_set::
erase(const_iterator const it) noexcept
{
    return m_entries.erase(it);
}

trash_entry_set::iterator trash_entry_set::
erase(const_iterator const b, const_iterator const e) noexcept
{
    return m_entries.erase(b, e);
}

} // namespace stream9::xdg::trash
