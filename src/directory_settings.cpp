#include <stream9/xdg/trash/directory_settings.hpp>

#include <stream9/xdg/trash/environment.hpp>
#include <stream9/xdg/trash/error.hpp>

#include <stream9/from_string.hpp>
#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/safe_integer.hpp>
#include <stream9/xdg/settings/scanner.hpp>

namespace stream9::xdg::trash {

using path::operator/;

constexpr string_view
setting_file_name()
{
    return ".directory";
}

constexpr string_view
section_name()
{
    return "stream9::trash";
}

constexpr string_view
size_limit_key()
{
    return "SizeLimit";
}

static file_size_t
calculate_size_limit(file_size_t const capacity, double const proportion)
{
    return static_cast<std::int64_t>(
                    static_cast<double>(capacity) * proportion );
}

constexpr file_size_t
default_size_limit()
{
    return 1_gib;
}

static file_size_t
default_size_limit(file_size_t const capacity)
{
    return calculate_size_limit(capacity, 0.01);
}

static file_size_t
default_size_limit(string_view p)
{
    try {
        auto const cap = env().filesystem_capacity(p);

        return default_size_limit(cap);
    }
    catch (...) {
        log::warn() << "fail to get capacity of the filesystem: " << p;

        return default_size_limit();
    }
}

static file_size_t
parse_size_limit(string_view s, string_view path) noexcept
{
    file_size_t cap;
    try {
        cap = env().filesystem_capacity(path);
    }
    catch (...) {
        log::warn() << "fail to get capacity of the filesystem: " << path;
        return default_size_limit();
    }

    if (s.empty()) return default_size_limit(cap);

    try {
        auto const proportion = st9::from_string<double>(s);
        if (proportion > 1.0) {
            log::warn() << "field \"SizeLimit\" > 1.0: " << path;

            return default_size_limit(cap);
        }
        else if (proportion <= 0) {
            log::warn() << "field \"SizeLimit\" <= 0: " << path;

            return default_size_limit(cap);
        }
        else {
            return calculate_size_limit(cap, proportion);
        }
    }
    catch (...) {
        log::warn() << "fail to parse field \"SizeLimit\": " << s;

        return default_size_limit(cap);
    }
}

struct directory_settings_parser
{
    using pos_t = safe_integer<ptrdiff_t, 0>;
    using next_action = xdg::settings::next_action;
    enum class state { initial, scanning, end };

    string_view m_path;

    enum state m_state = state::initial;
    string m_text;
    opt<file_size_t> m_size_limit {};

    directory_settings_parser(string_view p)
        : m_path { p }
    {}

    opt<file_size_t> operator()()
    {
        try {
            m_text = env().load_file(m_path, 5_mib);
        }
        catch (...) {
            return m_size_limit;
        }

        xdg::settings::scan(m_text, *this);
        return m_size_limit;
    }

    // xdg::settings::content_handler
    void on_group_header(string_view const s, next_action& a) noexcept
    {
        if (s == section_name() && m_state == state::initial) {
            m_state = state::scanning;
        }
        else {
            a = next_action::skip_group;
        }
    }

    void on_entry(string_view const key,
                  string_view const value, next_action&) noexcept
    {
        if (m_state != state::scanning) return;

        if (key == size_limit_key()) {
            m_size_limit = parse_size_limit(value, m_path);
            m_state = state::end;
        }
    }

    void on_comment(string_view, next_action&) noexcept {}

    void on_new_line(string_view::iterator) noexcept {}

    void on_error(std::error_code ec, string_view r, next_action&) noexcept
    {
        pos_t const pos = r.begin() - m_text.data();

        log::warn() << "parse error on " << m_path << " at " << pos << ":"
                    << ec.message();
    }
};

struct impl
{
    directory_settings* m_p;

    impl(directory_settings& p) : m_p { &p } {}

    void load_settings()
    {
        try {
            auto const file_path = m_p->m_path / setting_file_name();

            directory_settings_parser parse { file_path };

            auto const o_limit = parse();

            m_p->m_size_limit = o_limit ? *o_limit
                                 : default_size_limit(m_p->m_path);
        }
        catch (...) {
            throw_error(errc::fail_to_load_directory_settings, {
                { "path", m_p->m_path / setting_file_name() },
            });
        }
    }
};

/*
 * class directory_settings
 */
static_assert(!std::copyable<directory_settings>);
static_assert(!std::movable<directory_settings>);

struct directory_settings_impl : directory_settings
{
    directory_settings_impl(string_view dir_path)
        : directory_settings { dir_path }
    {}
};

shared_node<directory_settings> directory_settings::
construct(string_view dir_path)
{
    try {
        shared_node<directory_settings_impl> result { dir_path };

        env().add_directory_observer(result->m_path, result);

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

directory_settings::
directory_settings(string_view dir_path)
    : m_path { dir_path }
{
    try {
        if (env().is_directory(m_path)) {
            impl(*this).load_settings();
        }
        else {
            m_size_limit = default_size_limit();
        }
    }
    catch (...) {
        rethrow_error();
    }
}

#if 0
void directory_settings::
set_size_limit(file_size_t const v) noexcept
{
    m_size_limit = v;
}
#endif

void directory_settings::
file_created(string_view, string_view filename) noexcept
{
    try {
        if (filename == setting_file_name()) {
            impl(*this).load_settings();
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void directory_settings::
file_modified(string_view, string_view filename) noexcept
{
    try {
        if (filename == setting_file_name()) {
            impl(*this).load_settings();
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void directory_settings::
file_deleted(string_view, string_view filename) noexcept
{
    try {
        if (filename == setting_file_name()) {
            impl(*this).load_settings();
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

} // namespace stream9::xdg::trash
