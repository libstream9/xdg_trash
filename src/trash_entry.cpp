#include <stream9/xdg/trash/trash_entry.hpp>

#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>
#include <stream9/xdg/trash/trash_info.hpp>

#include <concepts>

namespace stream9::xdg::trash {

static_assert(std::copyable<trash_entry>);
static_assert(std::equality_comparable<trash_entry>);

trash_entry::
trash_entry(string name,
            class trash_directory& trash_dir,
            class trash_info& info)
    : m_name { std::move(name) }
    , m_trash_dir { &trash_dir }
    , m_info { &info }
{}

file_size_t trash_entry::
file_size() const
{
    try {
        return m_trash_dir->file_size(*m_info);
    }
    catch (...) {
        rethrow_error();
    }
}

file_time_t trash_entry::
last_modified_time() const
{
    try {
        return env().last_write_time(m_info->file_path());
    }
    catch (...) {
        rethrow_error();
    }
}

file_time_t trash_entry::
deletion_time() const noexcept
{
    return m_info->deletion_date();
}

bool trash_entry::
operator==(trash_entry const& other) const noexcept
{
    return m_name == other.m_name;
}

} // namespace stream9::xdg::trash
