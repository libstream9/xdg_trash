#include <stream9/xdg/trash/linux_environment.hpp>

#include <stream9/xdg/trash/error.hpp>

#include <ranges>

#include <stdlib.h> // mkstemp
#include <unistd.h> // getuid

#include <stream9/array.hpp>
#include <stream9/bits.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/emplace_back.hpp>
#include <stream9/erase_if.hpp>
#include <stream9/filesystem/size_recursive.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/json.hpp>
#include <stream9/less.hpp>
#include <stream9/libmount.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/linux/epoll.hpp>
#include <stream9/linux/exec.hpp>
#include <stream9/linux/fork.hpp>
#include <stream9/linux/inotify.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/linux/open.hpp>
#include <stream9/linux/realpath.hpp>
#include <stream9/linux/remove.hpp>
#include <stream9/linux/rename.hpp>
#include <stream9/linux/rmdir.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/linux/statvfs.hpp>
#include <stream9/linux/unlink.hpp>
#include <stream9/linux/wait.hpp>
#include <stream9/log.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/path/home_directory.hpp>
#include <stream9/projection.hpp>
#include <stream9/sorted_array.hpp>
#include <stream9/strings/starts_with.hpp>
#include <stream9/to_string.hpp>
#include <stream9/xdg/base_dir.hpp> // data_home()

namespace stream9::xdg::trash {

using path::operator/;

constexpr uint32_t mount_event = 1;
constexpr uint32_t inotify_event = 2;

static bool
is_relevant_fstype(auto const& fs)
{
    auto const o_fstype = fs.fstype();
    if (!o_fstype) {
        log::dbg() << "mount entry without fstype: "
                   << json::value_from(fs);
        return false;
    }

    if (*o_fstype == "fuse.trashfs") return false;

    return !fs.is_pseudofs() || str::starts_with(*o_fstype, "fuse.");
}

struct linux_environment::impl
{
    struct fs_event {
        enum event_type { create, modify };

        enum event_type type;
        int wd;
        string filename;
        file_time_t timestamp;
    };

    struct watch {
        int wd;
        string path;
        weak_node<directory_observer> observer;
    };

    using mount_table = stream9::libmount::table;
    using event_monitor = stream9::linux::epoll<>;
    using mount_monitor = stream9::libmount::monitor;
    using directory_monitor = stream9::linux::inotify;
    using directory_observers = sorted_array<watch, less, project<&watch::wd>>;

    linux_environment* m_p;
    array<fs_event> m_events;
    directory_observers m_dir_observers;
    event_monitor m_epoll;
    mount_table m_mtab;
    mount_monitor m_mount_monitor;
    directory_monitor m_inotify;
    observer_set<mount_observer> m_mount_observers;

    impl(linux_environment& p)
        : m_p { &p }
    {
        m_mtab.parse_mtab();

        m_mount_monitor.enable_kernel();
        m_mount_monitor.enable_userspace();

        m_epoll.add(m_mount_monitor.fd(), {
            .events = EPOLLIN,
            .data = { .u32 = mount_event },
        });

        m_epoll.add(m_inotify.fd(), {
            .events = EPOLLIN,
            .data = { .u32 = inotify_event },
        });
    }

    void
    insert_event(fs_event::event_type t, int wd, string_view name)
    {
        using std::ranges::find_if;

        try {
            auto it = find_if(m_events,
                [&](auto&& e) {
                    return e.wd == wd
                        && e.filename  == name;
                });
            if (it != m_events.end()) {
                if (it->type != fs_event::create) {
                    it->type = t;
                }
                it->timestamp = clock_t::now();
            }
            else {
                st9::emplace_back(m_events,
                    t,
                    wd,
                    string(name),
                    clock_t::now()
                );
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    auto
    find_event(int wd, string_view name) noexcept
    {
        using std::ranges::find_if;

        return find_if(m_events,
            [&](auto&& e) {
                return e.wd == wd
                    && e.filename == name;
            });
    }

    auto
    erase_event(int wd, string_view name) noexcept
    {
        return st9::erase_if(m_events,
            [&](auto&& e) {
                return e.wd == wd
                    && e.filename == name;
            });
    }

    void process_mount_event()
    {
        try {
            mount_table new_mtab;
            new_mtab.parse_mtab();

            stream9::libmount::tabdiff diffs { m_mtab, new_mtab };

            m_mount_monitor.cleanup();

            for (auto const& d: diffs) {
                if (d.oper == MNT_TABDIFF_MOUNT) {
                    if (d.new_fs && is_relevant_fstype(*d.new_fs)) {
                        if (auto const& o_target = d.new_fs->target())  {
                            string root { o_target->c_str() };

                            m_mount_observers.for_each([&](auto& ob) {
                                ob.filesystem_mounted(root);
                            });
                        }
                    }
                }
                else if (d.oper == MNT_TABDIFF_UMOUNT) {
                    if (d.old_fs && is_relevant_fstype(*d.old_fs)) {
                        if (auto const& o_target = d.old_fs->target())  {
                            string root { o_target->c_str() };

                            m_mount_observers.for_each([&](auto& ob) {
                                ob.filesystem_unmounted(root);
                            });
                        }
                    }
                }
            }

            using std::ranges::swap;
            swap(m_mtab, new_mtab);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void process_inotify_event()
    {
        try {
            char buf[1024];

            for (auto const& ev: m_inotify.read(buf)) {
                try {
                    auto const& name = lx::name(ev);
                    auto const& mask = ev.mask;
                    if (bits::any_of(mask, IN_CREATE)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: create event without name";
                            continue;
                        }

                        insert_event(fs_event::create, ev.wd, name);
                    }
                    else if (bits::any_of(mask, IN_DELETE | IN_MOVED_FROM)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: delete event without name";
                            continue;
                        }

                        auto n = erase_event(ev.wd, name);
                        if (n == 0) {
                            emit_inotify_event(ev.wd,
                                [&](auto& p, auto& ob) {
                                    ob.file_deleted(p, name);
                                });
                        }

                    }
                    else if (bits::includes(mask, IN_ATTRIB)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: attrib event without name";
                            continue;
                        }

                        emit_inotify_event(ev.wd,
                            [&](auto& p, auto& ob) {
                                ob.file_modified(p, name);
                            });
                    }
                    else if (bits::includes(mask, IN_MODIFY)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: modify event without name";
                            continue;
                        }

                        insert_event(fs_event::modify, ev.wd, name);
                    }
                    else if (bits::includes(mask, IN_MOVED_TO)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: moved_to event without name";
                            continue;
                        }

                        insert_event(fs_event::create, ev.wd, name);
                    }
                    else if (bits::includes(mask, IN_CLOSE_WRITE)) {
                        if (name.empty()) {
                            log::dbg() << "inotify: close_write event without name";
                            continue;
                        }

                        auto it = find_event(ev.wd, name);
                        if (it != m_events.end()) {
                            if (it->type == fs_event::create) {
                                emit_inotify_event(ev.wd,
                                    [&](auto& p, auto& ob) {
                                        ob.file_created(p, name);
                                    });
                            }
                            else {
                                assert(it->type == fs_event::modify);
                                emit_inotify_event(ev.wd,
                                    [&](auto& p, auto& ob) {
                                        ob.file_modified(p, name);
                                    });
                            }
                            m_events.erase(it);
                        }
                        else {
                            // 1) file is opened for write then closed without modification.
                            // 2) kernel didn't send CREATE or MODIFY event.
                            emit_inotify_event(ev.wd,
                                [&](auto& p, auto& ob) {
                                    ob.file_modified(p, name);
                                });
                        }
                    }
                    else if (bits::any_of(mask, IN_DELETE_SELF | IN_MOVE_SELF
                                                               | IN_UNMOUNT) )
                    {
                        emit_inotify_event(ev.wd,
                            [](auto& p, auto& ob) {
                                ob.directory_disappeared(p);
                            });
                    }
                }
                catch (...) {
                    print_error(log::warn());
                }
            }

            emit_old_events();
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    emit_old_events()
    {
        using namespace std::literals;

        auto three_sec_ago = clock_t::now() - 3s;

        st9::erase_if(m_events,
            [&](auto& ev) {
                if (ev.timestamp < three_sec_ago) {
                    if (ev.type == fs_event::create) {
                        emit_inotify_event(ev.wd,
                            [&](auto& p, auto& ob) {
                                ob.file_created(p, ev.filename);
                            });
                    }
                    else {
                        assert(ev.type == fs_event::modify);
                        emit_inotify_event(ev.wd,
                            [&](auto& p, auto& ob) {
                                ob.file_modified(p, ev.filename);
                            });
                    }
                    return true;
                }
                else {
                    return false;
                }
            }
        );
    }

    void
    emit_inotify_event(int wd, auto op) noexcept
    {
        try {
            auto it = m_dir_observers.find(wd);
            if (it == m_dir_observers.end()) return;

            if (auto ob = it->observer.lock()) {
                try {
                    auto const& path = it->path;
                    op(path, **ob);
                }
                catch (...) {
                    print_error(log::warn());
                }
            }
            else {
                try {
                    m_inotify.rm_watch(wd);
                }
                catch (error const& e) {
                    if (e.why() == lx::errc::einval) {
                        // wd might be removed automatically
                    }
                    else {
                        throw;
                    }
                }
                m_dir_observers.erase(it);
            }
        }
        catch (...) {
            print_error(log::warn());
        }
    }
};

/*
 * class linux_environment
 */
linux_environment::
linux_environment()
    try : m_impl { *this }
{}
catch (...) {
    rethrow_error();
}

linux_environment::~linux_environment() = default;

::uid_t linux_environment::
uid() noexcept
{
    return ::getuid();
}

string linux_environment::
home_directory()
{
    try {
        return path::home_directory();
    }
    catch (...) {
        rethrow_error();
    }
}

string linux_environment::
home_data_directory()
{
    try {
        return xdg::data_home();
    }
    catch (...) {
        rethrow_error();
    }
}

array<string> linux_environment::
mounted_top_directories()
{
    try {
        array<string> result;

        for (auto const& fs: m_impl->m_mtab) {
            if (!is_relevant_fstype(fs)) continue;

            auto const o_target = fs.target();
            if (!o_target) {
                log::dbg() << "mount entry without target: "
                           << json::value_from(fs);
                continue;
            }

            st9::emplace_back(result, o_target->c_str());
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

string linux_environment::
top_directory(string_view p)
{
    auto abs = path::to_absolute(p);
    auto const o_fs = m_impl->m_mtab.find_mountpoint(abs, MNT_ITER_BACKWARD);
    if (o_fs) {
        auto const o_tgt = o_fs->target();
        if (o_tgt) {
            return o_tgt->c_str();
        }
    }

    throw error {
        errc::fail_to_find_top_directory,
        { { "path", p } }
    };
}

string linux_environment::
absolute_path(string_view p)
{
    try {
        return path::to_absolute(p);
    }
    catch (...) {
        rethrow_error({
            { "p", p }
        });
    }
}

string linux_environment::
real_path(string_view p)
{
    try {
        return lx::realpath(p);
    }
    catch (...) {
        rethrow_error({
            { "p", p }
        });
    }
}

int linux_environment::
monitor_fd() const noexcept
{
    return m_impl->m_epoll.fd();
}

bool linux_environment::
exists(string_view p)
{
    try {
        auto o_st = lx::nothrow::stat(p);
        if (o_st) {
            return true;
        }
        else if (o_st == lx::enoent) {
            return false;
        }
        else {
            throw error {
                o_st.error(), {
                    { "p", p }
                }
            };
        }
    }
    catch (...) {
        rethrow_error({
            { "p", p }
        });
    }
}

bool linux_environment::
is_directory(string_view p) noexcept
{
    auto o_stat = lx::nothrow::stat(p);
    if (o_stat && lx::is_directory(*o_stat)) {
        return true;
    }
    else {
        return false;
    }
}

bool linux_environment::
is_directory_with_sticky_bit(string_view p) //noexcept
{
    auto o_stat = lx::nothrow::stat(p);
    return o_stat
        && lx::is_directory(*o_stat)
        && lx::has_sticky_bit(*o_stat);
}

void linux_environment::
list_directory(string_view dir_path,
               function_ref<bool(string_view)> cb)
{
    try {
        lx::directory dir { dir_path };
        for (auto const& ent: dir) {
            auto name = lx::name(ent);
            if (name == "." || name == "..") continue;
            if (!cb(dir_path / name)) break;
        }
    }
    catch (...) {
        throw_error(errc::fail_to_access_file, {
            { "dir_path", dir_path }
        });
    }
}

static nanoseconds
to_duration(struct ::timespec const& ts) noexcept
{
    return nanoseconds {
        ts.tv_sec * 1000000000 + ts.tv_nsec
    };
}

file_time_t linux_environment::
current_time()
{
    return clock_t::now();
}

file_time_t linux_environment::
last_write_time(string_view pathname)
{
    try {
        auto stat = lx::stat(pathname);
        return file_time_t { to_duration(stat.st_mtim) };
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

file_size_t linux_environment::
file_or_directory_size(cstring_ptr const& path)
{
    try {
        return fs::size_recursive(path);
    }
    catch (...) {
        rethrow_error({
            { "path", path },
        });
    }
}

file_size_t linux_environment::
filesystem_capacity(string_view p)
{
    try {
        auto st = lx::statvfs(p);
        return lx::total_bytes(st);
    }
    catch (...) {
        rethrow_error();
    }
}

string linux_environment::
load_file(string_view p, file_size_t size_limit)
{
    try {
        return fs::load_string(p, size_limit);
    }
    catch (...) {
        rethrow_error({
            { "p", p },
            { "size limit", size_limit },
        });
    }
}

void linux_environment::
add_mount_observer(weak_node<mount_observer> ob)
{
    try {
        m_impl->m_mount_observers.insert(ob);
    }
    catch (...) {
        rethrow_error();
    }
}

void linux_environment::
add_directory_observer(string_view p,
                       weak_node<directory_observer> ob)
{
    try {
        auto const mask = IN_CREATE |
                          IN_DELETE | IN_MOVED_FROM | IN_MOVED_TO |
                          IN_MODIFY | IN_ATTRIB |
                          IN_CLOSE_WRITE |
                          IN_DELETE_SELF | IN_MOVE_SELF;

        auto const wd = m_impl->m_inotify.add_watch(p, mask);

        // duplicated observer would not be a problem even if there is a such thing
        m_impl->m_dir_observers.emplace(wd, string(p), ob);
    }
    catch (...) {
        rethrow_error({ { "path", p } });
    }
}

void linux_environment::
move_file(string_view from_path, string_view to_path)
{
    try {
        auto pid = lx::fork();
        if (pid == 0) {
            lx::execp("mv", { string_view("mv"), from_path, to_path });
        }
        else {
            int wstatus = 0;
            lx::waitpid(pid, &wstatus);
            if (!WIFEXITED(wstatus)) {
                throw error { errc::fail_to_move_file };
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "from_path", from_path },
            { "to_path", to_path },
        });
    }
}

void linux_environment::
remove_file(string_view pathname)
{
    try {
        lx::remove(pathname);
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

void linux_environment::
remove_file_recursively(string_view p)
{
    try {
        auto st = lx::stat(p);
        if (lx::is_directory(st)) {
            lx::directory dir { p };
            for (auto const& ent: dir) {
                auto name = lx::name(ent);
                if (name == "." || name == "..") continue;
                auto p2 = p / name;
                remove_file_recursively(p2);
            }
            lx::rmdir(p);
        }
        else {
            lx::remove(p);
        }
    }
    catch (...) {
        rethrow_error({
            { "p", p }
        });
    }
}

void linux_environment::
create_directory(string_view pathname)
{
    try {
        cstring_ptr p { pathname };
        auto o_st = lx::nothrow::stat(p);
        if (o_st) {
            if (lx::is_directory(*o_st)) return;
            else {
                throw error { o_st.error() };
            }
        }
        else if (o_st == lx::enoent) {
            auto parent = path::dirname(p);
            create_directory(parent);
            lx::mkdir(p, 0755);
        }
        else {
            throw error { o_st.error() };
        }
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

ofstream linux_environment::
create_file(string_view pathname)
{
    try {
        auto fd = lx::open(pathname, O_WRONLY | O_CREAT | O_EXCL, 0666);

        ofstream os { std::move(fd) };
        os.exceptions(os.failbit);

        return os;
    }
    catch (...) {
        rethrow_error({
            { "pathname", pathname }
        });
    }
}

ofstream linux_environment::
make_temporary_file(string_view prefix)
{
    try {
        string tmpl { prefix };
        tmpl.append("XXXXXX");

        auto rv = ::mkstemp(tmpl.data());
        if (rv == -1) {
            throw error {
                lx::make_error_code(errno), {
                    { "template", tmpl }
                }
            };
        }

        return lx::fd { rv };
    }
    catch (...) {
        rethrow_error({
            { "prefix", prefix }
        });
    }
}

void linux_environment::
process_events(milliseconds timeout/*-1*/)
{
    try {
        for (auto const& ev: m_impl->m_epoll.wait(timeout)) {
            try {
                switch (ev.data.u32) {
                    case mount_event:
                        m_impl->process_mount_event();
                        break;
                    case inotify_event:
                        m_impl->process_inotify_event();
                        break;
                }
            }
            catch (...) {
                print_error(log::warn());
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "timeout", st9::to_string(timeout) }
        });
    }
}

static opt<shared_node<environment>> s_env;

environment&
env()
{
    try {
        if (!s_env) {
            s_env = shared_node<linux_environment>();
        }
        return *s_env;
    }
    catch (...) {
        rethrow_error();
    }
}

void
set_env(shared_node<environment> env) noexcept
{
    s_env = env;
}

} // namespace stream9::xdg::trash
