#include <stream9/xdg/trash/trash_info_parser.hpp>

#include <stream9/xdg/trash/error.hpp>

#include <ctime>

#include <stream9/cstring_ptr.hpp>
#include <stream9/from_string.hpp>
#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/optional.hpp>
#include <stream9/xdg/settings/scanner.hpp>

namespace stream9::xdg::trash::trash_info_parser {

namespace settings = stream9::xdg::settings;

struct parser
{
    using next_action = settings::next_action;

    opt<string_view> m_path;
    opt<deletion_date_t> m_deletion_date;

    opt<string_view> m_text;
    warning_cb m_warning_cb;
    opt<string_view> m_header;
    std::size_t m_line_no = 0;
    string_view::iterator m_bol;

    // command
    void
    run(string_view text, warning_cb cb)
    {
        m_text = text;
        m_warning_cb = cb;
        m_bol = m_text->begin();

        settings::scan(*m_text, *this);

        if (!m_header) {
            throw error {
                errc::no_trash_info_group, {
                    { "line no.", m_line_no },
                    { "column", m_text->end() - m_bol }
                }
            };
        }

        if (!m_path) {
            throw error {
                errc::no_path_entry, {
                    { "line no.", m_line_no },
                    { "column", m_header->begin() - m_bol }
                }
            };
        }

        if (!m_deletion_date) {
            throw error {
                errc::no_deletion_date_entry, {
                    { "line no.", m_line_no },
                    { "column", m_header->begin() - m_bol }
                }
            };
        }
    }

    // hooks for xdg::settings::scanner
    void
    on_group_header(string_view s, next_action& a) noexcept
    {
        if (m_header) {
            m_warning_cb(errc::multiple_trash_info_group,
                         m_line_no,
                         s.begin() - m_bol);

            a = next_action::skip_group;
            return;
        }

        if (s == "Trash Info") {
            m_header = s;
        }
    }

    void
    on_entry(string_view key, string_view value, next_action&)
    {
        if (!m_header) {
            m_warning_cb(errc::non_grouped_entry,
                         m_line_no,
                         key.begin() - m_bol);
            return;
        }

        if (key == "Path") {
            if (m_path) {
                m_warning_cb(errc::multiple_path_entry,
                             m_line_no,
                             key.begin() - m_bol);
            }
            if (value.empty()) {
                throw error {
                    errc::empty_path_entry, {
                        { "line no.", m_line_no },
                        { "column", value.begin() - m_bol }
                    }
                };
            }
            m_path = value;
        }
        else if (key == "DeletionDate") {
            if (m_deletion_date) {
                m_warning_cb(errc::multiple_deletion_date_entry,
                             m_line_no,
                             key.begin() - m_bol);
            }
            if (value.empty()) {
                throw error {
                    errc::empty_deletion_date_entry, {
                        { "line no.", m_line_no },
                        { "column", value.begin() - m_bol }
                    }
                };
            }
            m_deletion_date = parse_local_time(value);
        }
    }

    void on_comment(string_view, next_action&) {}

    void
    on_new_line(string_view::iterator bol) noexcept
    {
        ++m_line_no;
        m_bol = bol;
    }

    void
    on_error(std::error_code ec, string_view r, next_action&)
    {
        throw error {
            ec, {
                { "line no.", m_line_no },
                { "column", r.begin() - m_bol }
            }
        };
    }

private:
    deletion_date_t
    parse_local_time(string_view s) const
    {
        try {
            cstring_ptr cs { s };

            return str::from_string<deletion_date_t>(cs.data(), "%FT%T");
        }
        catch (...) {
            throw error {
                errc::invalid_time_string, {
                    { "line no.", m_line_no },
                    { "column", s.begin() - m_bol }
                }
            };
        }
    }
};

fields
parse(string_view text, warning_cb cb)
{
    parser p;
    p.run(text, cb);

    return {
        *p.m_path,
        *p.m_deletion_date,
    };
}

std::error_category&
error_category()
{
    static struct impl : std::error_category {
        char const* name() const noexcept
        {
            return "stream9::trash::trash_info_parser";
        }

        std::string message(int e) const
        {
            switch (static_cast<errc>(e)) {
                using enum errc;
                case no_trash_info_group:
                    return "there is not trash info group";
                case no_path_entry:
                    return "there is not Path entry";
                case no_deletion_date_entry:
                    return "there is not DeletionDate entry";
                case empty_path_entry:
                    return "value of Path is empty";
                case invalid_time_string:
                    return "there is invalid DeletionDate value";
                case empty_deletion_date_entry:
                    return "value of DeletionDate is empty";
                case multiple_trash_info_group:
                    return "there are multiple TrashInfo group";
                case non_grouped_entry:
                    return "there is entry at outside of TrashInfo group";
                case multiple_path_entry:
                    return "there are multiple Path entry in TrashInfo group";
                case multiple_deletion_date_entry:
                    return "there are multiple DeletionDate entry in TrashInfo group";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::xdg::trash::trash_info_parser
