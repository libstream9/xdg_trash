#include <stream9/xdg/trash/trash_directory.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/error.hpp>

#include <algorithm>

#include <stream9/array.hpp>
#include <stream9/container/remove_const.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/log.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/path/dirname.hpp>
#include <stream9/strings/ends_with.hpp>

namespace stream9::xdg::trash {

using path::operator/;

static auto
create_trash_info(string_view info_dir, string_view file_path)
{
    try {
        return node<trash_info>(info_dir, file_path);
    }
    catch (...) {
        throw_error(errc::fail_to_create_trash_info);
    }
}

static auto
load_trash_info(string_view info_path)
{
    try {
        return node<trash_info>(info_path);
    }
    catch (...) {
        rethrow_error();
    }
}

static void
create_trash_directory(string_view p)
{
    try {
        env().create_directory(p);
    }
    catch (...) {
        throw_error(errc::fail_to_create_trash_directory, {
            { "path", p }
        });
    }
}

static void
create_sub_directories(string_view p)
{
    try {
        env().create_directory(p / "info");
        env().create_directory(p / "files");
    }
    catch (...) {
        throw_error(errc::fail_to_create_trash_directory, {
            { "path", p }
        });
    }
}

static bool
directory_exists(string_view p)
{
    try {
        return env().is_directory(p);
    }
    catch (...) {
        throw_error(errc::fail_to_access_file, {
            { "path", p },
        });
    }
}

static bool
file_exists(string_view p)
{
    try {
        return env().exists(p);
    }
    catch (...) {
        throw_error(errc::fail_to_access_file, {
            { "path", p },
        });
    }
}

static auto
sort_by_deletion_time_descending(trash_directory const& trash_dir)
{
    using std::ranges::sort;
    try {
        array<trash_info> result { trash_dir };

        sort(result, [&](auto&& i1, auto&& i2) {
            return i1.deletion_date() > i2.deletion_date();
        });

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static void
open_space_for_new_trash(trash_directory& trash_dir,
                         file_size_t const size_limit)
{
    try {
        auto const& infos = sort_by_deletion_time_descending(trash_dir);

        auto it = infos.begin();
        file_size_t dir_size = 0;
        for (; it != infos.end(); ++it) {
            dir_size += trash_dir.file_size(*it);

            if (dir_size > size_limit) break;
        }

        for (; it != infos.end(); ++it) {
            try {
                trash_dir.delete_trash(*it);
            }
            catch (...) {
                print_error(log::warn(), {
                    { "directory path", trash_dir.path() },
                    { "trash_info", *it },
                });
            }
        }
    }
    catch (...) {
        throw_error(errc::fail_to_open_space_for_new_trash, {
            { "size limit", size_limit },
        });
    }
}

static string
info_directory_path(trash_directory const& d)
{
    try {
        return d.path() / "info";
    }
    catch (...) {
        rethrow_error();
    }
}

struct impl
{
    trash_directory* m_p; // non-null

    impl(trash_directory& d)
        : m_p { &d }
    {}

    void
    scan_info_directory(string_view path)
    {
        try {
            try {
                env().list_directory(path / "info",
                    [&](auto&& p) {
                        if (str::ends_with(p, ".trashinfo")) {
                            try {
                                insert_trash_info(load_trash_info(p));
                            }
                            catch (...) {
                                auto ec = err::current_error_code();
                                log::warn() << "ignoring invalid trashinfo file:" << p
                                            << ":" << std::quoted(ec.message());
                            }
                        }
                        return true;
                    });
            }
            catch (error const& e) {
                if (e.why() != errc::fail_to_access_file) {
                    throw;
                }
            }
        }
        catch (...) {
            throw_error(errc::fail_to_scan_trash_directory, {
                { "path", path }
            });
        }
    }

    void
    reload_trash_info(string_view p)
    {
        try {
            auto it1 = m_p->find_info_by_path(p);
            if (it1 != m_p->end()) {
                if (it1->reload()) {
                    update_directory_size_cache(*it1);

                    m_p->m_observers.for_each([&](auto&& ob) {
                        ob.trash_info_modified(*m_p, *it1);
                    });
                }
            }
            else {
                insert_trash_info(load_trash_info(p));
            }
        }
        catch (...) {
            throw_error(errc::fail_to_load_trash_info, {
                { "directory path", m_p->m_path },
                { "trashinfo path", p },
            });
        }
    }

    trash_directory::iterator
    insert_trash_info(node<trash_info> i)
    {
        try {
            auto [it, ok] = m_p->m_entries.insert(std::move(i));
            if (ok) {
                insert_directory_size_cache(**it);

                m_p->m_observers.for_each([&](auto&& ob) {
                    ob.trash_info_created(*m_p, **it);
                });
            }
            else {
                throw_error(errc::fail_to_insert_trash_info);
            }

            return it;
        }
        catch (...) {
            throw_error(errc::fail_to_insert_trash_info, {
                { "directory path", m_p->m_path },
            });
        }
    }

    trash_directory::iterator
    delete_trash_info(trash_directory::const_iterator const it)
    {
        try {
            if (it == m_p->end()) {
                return con::remove_const(m_p->m_entries, it.base());
            }

            m_p->m_observers.for_each([&](auto&& ob) {
                ob.trash_info_deleted(*m_p, *it);
            });

            m_p->m_dir_sizes->erase_entry(*it);

            return m_p->m_entries.erase(it.base());
        }
        catch (...) {
            assert(it != m_p->end());

            throw_error(errc::fail_to_delete_trash_info, {
                { "directory path", m_p->m_path },
                { "trash_info", *it },
            });
        }
    }

    void
    insert_directory_size_cache(trash_info const& i)
    {
        try {
            m_p->m_dir_sizes->insert_or_update_entry(i);
        }
        catch (...) {
            throw_error(errc::fail_to_update_directory_size_cache, {
                { "path", m_p->m_path },
                { "trash_info", i },
            });
        }
    }

    void
    update_directory_size_cache(trash_info const& i)
    {
        try {
            m_p->m_dir_sizes->update_or_insert_entry(i);
        }
        catch (...) {
            throw_error(errc::fail_to_update_directory_size_cache, {
                { "path", m_p->m_path },
                { "trash_info", i },
            });
        }
    }
};

/*
 * class trash_directory
 */
static_assert(!std::copyable<trash_directory>);
static_assert(!std::movable<trash_directory>);
static_assert(std::ranges::random_access_range<trash_directory>);
static_assert(std::totally_ordered<trash_directory>);

struct trash_directory_impl : public trash_directory {
public:
    trash_directory_impl(string_view p,
                         option const o)
        : trash_directory { p, o }
    {}
};


shared_node<trash_directory> trash_directory::
construct(string_view p)
{
    return trash_directory::construct(p, option());
}

shared_node<trash_directory> trash_directory::
construct(string_view p , option const o)
{
    try {
        shared_node<trash_directory_impl> result { p, o };

        env().add_directory_observer(info_directory_path(result), result);

        return result;
    }
    catch (...) {
        rethrow_error({ { "path", p }, });
    }
}

trash_directory::
trash_directory(string_view path, option opt)
    : m_path { path }
    , m_dir_sizes { directory_size_cache::construct(*this) }
    , m_settings { directory_settings::construct(path) }
{
    try {
        if (opt.create_if_not_exist) {
            create_trash_directory(m_path);
        }

        if (!directory_exists(m_path)) {
            throw_error(errc::directory_does_not_exist);
        }

        impl(*this).scan_info_directory(m_path);
    }
    catch (...) {
        rethrow_error({
            { "path", path }
        });
    }
    // post-condition
    // assert(is_directory(m_path));
    // assert(is_writable(m_path));
    // subdirectories might not exist at this point
}

trash_directory::const_iterator trash_directory::
begin() const noexcept
{
    return m_entries.begin();
}

trash_directory::const_iterator trash_directory::
end() const noexcept
{
    return m_entries.end();
}

trash_directory::iterator trash_directory::
begin() noexcept
{
    return m_entries.begin();
}

trash_directory::iterator trash_directory::
end() noexcept
{
    return m_entries.end();
}

trash_directory::size_type trash_directory::
size() const noexcept
{
    return m_entries.size();
}

file_size_t trash_directory::
file_size(const_iterator it) const
{
    assert(it != m_entries.end());

    try {
        auto it2 = m_dir_sizes->find(it->name());
        if (it2 != m_dir_sizes->end()) {
            return it2->size;
        }
        else {
            return env().file_or_directory_size(it->file_path());
        }
    }
    catch (...) {
        rethrow_error({
            { "directory path", path() },
        });
    }
}

file_size_t trash_directory::
file_size(trash_info const& info) const
{
    try {
        auto it = find_info(info);
        assert(it != end());

        return file_size(it);
    }
    catch (...) {
        rethrow_error({
            { "directory path", path() },
            { "info", info },
        });
    }
}

file_size_t trash_directory::
directory_size() const
{
    try {
        file_size_t result {};

        for (auto it = begin(); it != end(); ++it) {
            result += file_size(it);
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "directory path", path() },
        });
    }
}

file_size_t trash_directory::
directory_size_limit() const noexcept
{
    return m_settings->size_limit();
}

trash_directory::const_iterator trash_directory::
find_info(trash_info const& info) const noexcept
{
    return st9::find(m_entries, info.name());
}

trash_directory::const_iterator trash_directory::
find_info_by_name(string_view const name) const noexcept
{
    return st9::find(m_entries, name);
}

trash_directory::const_iterator trash_directory::
find_info_by_path(string_view path) const noexcept
{
    return st9::find(m_entries, path, indirect<project<&trash_info::info_path>>());
}

trash_directory::iterator trash_directory::
find_info_by_path(string_view path) noexcept
{
    return st9::find(m_entries, path, indirect<project<&trash_info::info_path>>());
}

trash_info& trash_directory::
trash_file(string_view file_path)
{
    try {
        assert(!file_path.empty());

        if (!file_exists(file_path)) {
            throw_error(errc::file_does_not_exist);
        }

        auto const size = env().file_or_directory_size(file_path);
        if (size > directory_size_limit()) {
            throw_error(errc::file_is_too_big, {
                { "file size", size },
                { "directory size limit", directory_size_limit() },
            });
        }

        create_sub_directories(m_path);

        open_space_for_new_trash(*this, directory_size_limit() - size);

        auto info = create_trash_info(m_path / "info",
                                      env().absolute_path(file_path));
        auto const info_path = info->info_path();
        auto new_file_path = info->file_path();

        try {
            env().move_file(file_path, new_file_path);
        }
        catch (...) {
            throw_error(errc::fail_to_move_file);
        }

        try {
            auto const it = impl(*this).insert_trash_info(std::move(info));

            return *it;
        }
        catch (...) {
            env().move_file(new_file_path, file_path);
            env().remove_file(info_path);
            throw;
        }
    }
    catch (...) {
        rethrow_error({
            { "directory path", m_path },
            { "file path", file_path },
        });
    }
}

void trash_directory::
restore_trash(const_iterator const it)
{
    try {
        if (it == end()) return;

        auto const& original_path = it->original_path();

        if (env().exists(original_path)) {
            throw_error(errc::file_already_exist, {
                { "destination", original_path }
            });
        }

        try {
            env().create_directory(path::dirname(original_path));

            env().move_file(it->file_path(), original_path);
        }
        catch (...) {
            throw_error(errc::fail_to_move_file, {
                { "from", it->file_path() },
                { "to", original_path },
            });
        }

        try {
            env().remove_file(it->info_path());
        }
        catch (...) {
            env().move_file(original_path, it->file_path());

            throw_error(errc::fail_to_delete_file, {
                { "path", it->info_path() },
            });
        }

        impl(*this).delete_trash_info(it);
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_directory::
restore_trash(trash_info const& info)
{
    try {
        auto const it = find_info(info);
        restore_trash(it);
    }
    catch (...) {
        rethrow_error();
    }
}

trash_directory::iterator trash_directory::
delete_trash(const_iterator const it)
{
    try {
        if (it == end()) {
            return m_entries.erase(it.base(), it.base()); // just convert const_iterator -> iterator
        }

        string info_path { it->info_path() };
        string file_path { it->file_path() };

        auto rv = impl(*this).delete_trash_info(it);

        try {
            env().remove_file(info_path);
        }
        catch (...) {
            throw_error(errc::fail_to_delete_file, {
                { "file path", info_path }
            });
        }

        try {
            env().remove_file_recursively(file_path);
        }
        catch (...) {
            throw_error(errc::fail_to_delete_file, {
                { "file path", file_path }
            });
        }

        return rv;
    }
    catch (...) {
        json::object cxt { { "directory path", m_path } };
        if (it != end()) {
            cxt["info"] = *it;
        }

        rethrow_error(cxt);
    }
}

void trash_directory::
delete_trash(trash_info const& info)
{
    try {
        auto const it = find_info(info);

        delete_trash(it);
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_directory::
clear()
{
    try {
        for (auto it = begin(); it != end();) {
            it = delete_trash(it);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_directory::
add_observer(weak_node<observer> ob)
{
    try {
        m_observers.insert(ob);
    }
    catch (...) {
        rethrow_error();
    }
}

void trash_directory::
remove_observer(weak_node<observer> ob)
{
    try {
        m_observers.erase(ob);
    }
    catch (...) {
        rethrow_error();
    }
}

json::object trash_directory::
check_integrity()
{
    try {
        json::object rv;

        json::array entries;
        for (auto& e: m_entries) {
            auto o = e->check_integrity();
            if (!o.empty()) {
                entries.push_back(std::move(o));
            }
        }
        if (!entries.empty()) {
            rv["entries"] = std::move(entries);
        }

        auto cache = m_dir_sizes->check_integrity();
        if (!cache.empty()) {
            rv["directory size cache"] = std::move(cache);
        }

        json::array files_dir;
        try {
            auto files_dir_path = m_path / "files";
            for (auto const& ent: lx::directory(files_dir_path)) {
                auto filename = lx::name(ent);
                if (filename == "." || filename == "..") continue;

                auto info_path = m_path / "info" / filename;
                info_path += ".trashinfo";
                if (!env().exists(info_path)) {
                    files_dir.push_back(json::object {
                        { "description", "Trash file doesn't have corresponding info file" },
                        { "path", files_dir_path / filename },
                    });
                }
            }
        }
        catch (error const& e) {
            if (e.why() != lx::enoent) {
                files_dir.push_back(json::object {
                    { "description", "Fail to open trash file directory" },
                    { "error", e },
                });
            }
        }
        if (!files_dir.empty()) {
            rv["files directory"] = std::move(files_dir);
        }

        if (!rv.empty()) {
            rv["path"] = m_path;
        }

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

bool trash_directory::
operator==(trash_directory const& other) const noexcept
{
    return m_path == other.m_path;
}

std::strong_ordering trash_directory::
operator<=>(trash_directory const& other) const noexcept
{
    return m_path <=> other.m_path;
}

void trash_directory::
file_created(string_view, string_view filename) noexcept
{
    try {
        auto p = info_directory_path(*this) / filename;
        if (!str::ends_with(p, ".trashinfo")) return;

        impl(*this).reload_trash_info(p);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_directory::
file_modified(string_view, string_view filename) noexcept
{
    try {
        auto p = info_directory_path(*this) / filename;
        if (!str::ends_with(p, ".trashinfo")) return;

        impl(*this).reload_trash_info(p);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_directory::
file_deleted(string_view, string_view filename) noexcept
{
    try {
        auto p = info_directory_path(*this) / filename;
        if (!str::ends_with(p, ".trashinfo")) return;

        auto const it = find_info_by_path(p);
        if (it == end()) {
            return;
        }
        else {
            impl(*this).delete_trash_info(it);
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void trash_directory::
directory_disappeared(string_view) noexcept
{
    try {
        m_observers.for_each([&](auto&& ob) {
            ob.directory_disappeared(*this);
        });
    }
    catch (...) {
        print_error(log::warn());
    }
}

void
tag_invoke(json::value_from_tag, json::value& v, trash_directory const& d)
{
    auto& obj = v.emplace_object();

    json::value v1 { d.path() };
    v1 = d.path();

    obj["path"] = d.path();
    obj["size"] = d.size().value();

    json::array entries;
    for (auto const& ent: d) {
        entries.push_back(json::value_from(ent));
    }
    obj["entries"] = std::move(entries);
}

} // namespace stream9::xdg::trash
