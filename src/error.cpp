#include <stream9/xdg/trash/error.hpp>

#include <string>
#include <system_error>

#include <stream9/strings/stream.hpp>

namespace stream9::xdg::trash {

std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const*
        name() const noexcept override
        {
            return "stream9::trash::errc";
        }

        std::string
        message(int const e) const override
        {
            switch (static_cast<errc>(e)) {
                using enum errc;
                case file_does_not_exist:
                    return "file does not exist";
                case file_already_exist:
                    return "file already exist";
                case directory_does_not_exist:
                    return "directory does not exist";
                case trash_directory_does_not_exist:
                    return "trash directory does not exist";
                case file_is_too_big:
                    return "file is too big";
                case fail_to_access_file:
                    return "fail to access file";
                case fail_to_get_file_size:
                    return "fail to get file size";
                case fail_to_move_file:
                    return "fail to move file";
                case fail_to_delete_file:
                    return "fail to delete file";
                case fail_to_create_trash_directory:
                    return "fail to create trash directory";
                case fail_to_scan_trash_directory:
                    return "fail to scan trash directory";
                case fail_to_open_space_for_new_trash:
                    return "fail to open space for new trash";
                case fail_to_load_trash_info:
                    return "fail to load trash info";
                case fail_to_create_trash_info:
                    return "fail to create trash info";
                case fail_to_insert_trash_info:
                    return "fail to insert trash info";
                case fail_to_delete_trash_info:
                    return "fail to delete trash info";
                case fail_to_update_directory_size_cache:
                    return "fail to update directory size cache";
                case fail_to_create_top_directory:
                    return "fail to create top directory";
                case fail_to_find_top_directory:
                    return "fail to find path to the mountpoint of filesystem";
                case fail_to_save_directory_size_cache:
                    return "fail to save directory size cache";
                case fail_to_load_directory_settings:
                    return "fail to load directory settings";
                case fail_to_monitor_directory:
                    return "fail to monitor directory";
                case fail_to_monitor_mount:
                    return "fail to monitor mount";
                case invalid_file_extension:
                    return "invalid file extension";
                case parse_error:
                    return "parse error";
            }

            using str::operator<<;
            std::string s;
            s << "unknown error: " << e;

            return s;
        }
    } instance;

    return instance;
}

std::ostream&
operator<<(std::ostream& os, errc const ec)
{
    return os << error_category().message(static_cast<int>(ec));
}

} // namespace stream9::xdg::trash
