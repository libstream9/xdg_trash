#include <stream9/xdg/trash/trash_info.hpp>

#include <stream9/xdg/trash/environment.hpp>
#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/namespace.hpp>
#include <stream9/xdg/trash/trash_info_parser.hpp>

#include <cassert>
#include <concepts>

#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/basename.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/dirname.hpp>
#include <stream9/path/normalize.hpp>
#include <stream9/string.hpp>
#include <stream9/strings/ends_with.hpp>
#include <stream9/strings/remove_suffix.hpp>
#include <stream9/strings/stream.hpp>
#include <stream9/to_string.hpp>

namespace stream9::xdg::trash {

using path::operator/;

namespace  {

static string
format_time(file_time_t const& t)
{
    try {
        string result;

        return st9::to_string(t, "%FT%T");
    }
    catch (...) {
        rethrow_error();
    }
}

static string
make_trashinfo_filename(string_view filename, int n_try)
{
    try {
        string result { filename };

        if (n_try > 1) {
            result << " (" << (n_try - 1) << ")";
        }

        result.append(".trashinfo");

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

static auto
open_unique_info_file_for_write(string_view dir,
                                string_view filename)
{
    try {
        int n_try = 1;

        while (true) {
            auto info_path = dir / make_trashinfo_filename(filename, n_try);

            try {
                return std::make_pair(
                    env().create_file(info_path),
                    info_path
                );
            }
            catch (...) {
                if (n_try < 50) {
                    ++n_try;
                }
                else {
                    throw_error(errc::fail_to_create_trash_info, {
                        { "n_try", n_try }
                    });
                }
            }
        }
    }
    catch (...) {
        rethrow_error({
            { "dir", dir },
            { "filename", filename }
        });
    }
}

static void
create_trash_directory(string_view path)
{
    try {
        env().create_directory(path);
    }
    catch (...) {
        throw_error(errc::fail_to_create_trash_directory, {
            { "path", path }
        });
    }
}

static string
load_trash_info(string_view path)
{
    try {
        return env().load_file(path, 5_mib);
    }
    catch (...) {
        throw_error(errc::fail_to_load_trash_info, {
            { "path", path }
        });
    }
}

static auto
parse_trash_info(string_view text)
{
    json::array errors;
    try {
        auto result = trash_info_parser::parse(text,
            [&](auto& ec, auto line_no, auto column) {
                errors.push_back(json::object {
                    { "message", ec.message() },
                    { "line", line_no },
                    { "column", column },
                });
            });

        return result;
    }
    catch (...) {
        throw_error(errc::parse_error, {
            { "errors", std::move(errors) },
        });
    }
}

static string_view
extract_name_from_info_path(string_view path)
{
    try {
        auto n = path::basename(path);
        str::remove_suffix(n, ".trashinfo");
        return n;
    }
    catch (...) {
        rethrow_error({ { "path", path } });
    }
}

} // anonymous namespace

struct impl
{
    trash_info* m_p;

    impl(trash_info& p) : m_p { &p } {}

    void create_trash_info(string_view dir_path,
                           string_view file_path)
    {
        try {
            m_p->m_trash_dir_path = path::normalize(path::dirname(dir_path));

            auto [ofs, info_path] = open_unique_info_file_for_write(
                dir_path, path::basename(file_path)
            );

            m_p->m_name = extract_name_from_info_path(info_path);

            // According to the spec, "The system SHOULD support absolute pathnames
            // only in the “home trash” directory, not in the directories under $topdir."
            // On the other hand, spec also says relative path can be used only
            // original path that is "under" the trash directory.
            // So there is ambiguity regarding to whether I should use
            // relative path or absolute path.
            m_p->m_original_path = path::normalize(file_path);

            m_p->m_deletion_date = time_point_cast<seconds>(env().current_time());

            ofs << "[Trash Info]\n"
                << "Path=" << m_p->m_original_path << "\n"
                << "DeletionDate=" << format_time(m_p->m_deletion_date) << "\n";
            ofs.close();

            m_p->m_time_stamp = env().last_write_time(info_path);
        }
        catch (...) {
            throw_error(errc::fail_to_create_trash_info);
        }
    }
};

trash_info::
trash_info(string_view info_path)
{
    try {
        if (!str::ends_with(info_path, ".trashinfo")) {
            throw_error(errc::invalid_file_extension);
        }

        auto text = load_trash_info(info_path);

        auto fields = parse_trash_info(text);
        m_name = extract_name_from_info_path(info_path);
        m_trash_dir_path = path::to_absolute(
                                path::normalize(
                                    path::dirname(path::dirname(info_path)) ));
        m_original_path = path::normalize(fields.path);
        m_deletion_date = fields.deletion_date;

        m_time_stamp = env().last_write_time(info_path);

        if (!env().exists(file_path())) {
            throw_error(errc::file_does_not_exist, {
                { "file path", file_path() },
            });
        }
    }
    catch (...) {
        rethrow_error({ { "info_path", info_path } });
    }

    assert(path::is_absolute(m_trash_dir_path));
    assert(!m_name.empty());
    assert(!m_original_path.empty());
}

trash_info::
trash_info(string_view info_dir_path, string_view original_path)
    try
{
    assert(!info_dir_path.empty());
    assert(path::is_absolute(original_path));

    create_trash_directory(info_dir_path);

    impl(*this).create_trash_info(info_dir_path, original_path);

    assert(!m_name.empty());
    assert(path::is_absolute(m_trash_dir_path));
    assert(!m_original_path.empty());
}
catch (...) {
    rethrow_error({
        { "dir_path", info_dir_path },
        { "original_path", original_path }
    });
}

string trash_info::
info_path() const
{
    try {
        return m_trash_dir_path / "info" / m_name + ".trashinfo";
    }
    catch (...) {
        rethrow_error({
            { "trash_dir_path", m_trash_dir_path },
            { "name", m_name },
        });
    }
}

string trash_info::
file_path() const
{
    try {
        return m_trash_dir_path / "files" / m_name;
    }
    catch (...) {
        rethrow_error({
            { "trash_dir_path", m_trash_dir_path },
            { "name", m_name },
        });
    }
}

static bool
is_changed(trash_info const& lhs, trash_info const& rhs) noexcept
{
    return lhs.time_stamp() != rhs.time_stamp()
        || lhs.original_path() != rhs.original_path()
        || lhs.deletion_date() != rhs.deletion_date();
}

bool trash_info::
reload()
{
    try {
        trash_info new_info { info_path() };

        if (!is_changed(*this, new_info)) {
            return false;
        }
        else {
            *this = std::move(new_info);
            return true;
        }
    }
    catch (...) {
        rethrow_error({
            { "trash_dir_path", m_trash_dir_path },
            { "name", m_name },
        });
    }
}

json::object trash_info::
check_integrity()
{
    json::array errors;

    try {
        auto text = load_trash_info(info_path());
        auto [path, date] = parse_trash_info(text);

        if (m_original_path != path) {
            errors.push_back(json::object {
                { "description", "Original path is inconsistent with the info file." },
                { "memory", m_original_path },
                { "file", path },
            });
        }
        if (st9::to_string(m_deletion_date) != st9::to_string(date)) {
            errors.push_back(json::object {
                { "description", "Deletion date is inconsistent with the info file." },
                { "memory", st9::to_string(m_deletion_date) },
                { "file", st9::to_string(date) },
            });
        }

        auto ts = env().last_write_time(info_path());
        if (m_time_stamp != ts) {
            errors.push_back(json::object {
                { "description", "Time stamp is inconsistent with the info file." },
                { "memory", st9::to_string(m_time_stamp) },
                { "file", st9::to_string(ts) },
            });
        }

        if (!env().exists(file_path())) {
            errors.push_back(json::object {
                { "description", "Trash file on info file doesn't exists." },
                { "path", file_path() },
            });
        }
    }
    catch (...) {
        errors.push_back(std::current_exception());
    }

    json::object rv;
    if (!errors.empty()) {
        rv = json::object {
            { "trash_dir_path", m_trash_dir_path },
            { "name", m_name },
            { "errors", std::move(errors) },
        };
    }
    return rv;
}

/*
 * JSON support
 */
void
tag_invoke(json::value_from_tag, json::value& v, trash_info const& ti)
{
    v = json::object {
        { "type", "stream9::trash::trash_info" },
        { "name", ti.name() },
        { "info_path", ti.info_path() },
        { "file_path", ti.file_path() },
        { "original_path", ti.original_path() },
        { "deletion_date", str::to_string(ti.deletion_date()) },
        { "time_stamp", str::to_string(ti.time_stamp()) },
    };
}

} // namespace stream9::xdg::trash
