#include <stream9/xdg/trash/top_directory_set.hpp>

#include "namespace.hpp"

#include <stream9/xdg/trash/environment.hpp>
#include <stream9/xdg/trash/error.hpp>
#include <stream9/xdg/trash/top_directory.hpp>
#include <stream9/xdg/trash/trash_directory.hpp>

#include <algorithm>

#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/push_back.hpp>

namespace stream9::xdg::trash {

static auto
find_dir(auto& dir_set, string_view p) noexcept
{
    using std::ranges::find_if;
    return find_if(dir_set, [&](auto&& d) { return d->path() == p; });
}

static auto
emplace_dir(auto& dir_set, string_view path)
{
    auto const it = find_dir(dir_set, path);
    if (it != dir_set.end()) {
        return std::make_pair(it, false);
    }
    else {
        try {
            st9::push_back(dir_set, top_directory::construct(path));

            return std::make_pair(dir_set.end() - 1, true);
        }
        catch (...) {
            rethrow_error({ { "path", path } });
        }
    }
}

static void
erase_dir(auto& dir_set, top_directory& d) noexcept
{
    using std::ranges::remove_if;

    auto const r = remove_if(dir_set,
        [&](auto&& p) { return *p == d; });
    dir_set.erase(r.begin(), r.end());
}

/*
 * class top_directory_set
 */
static_assert(!std::copyable<top_directory_set>);
static_assert(!std::movable<top_directory_set>);
static_assert(std::ranges::random_access_range<top_directory_set>);

struct top_directory_set_impl : top_directory_set
{
    top_directory_set_impl() = default;
};

shared_node<top_directory_set> top_directory_set::
construct()
{
    try {
        shared_node<top_directory_set_impl> result;

        try {
            env().add_mount_observer(result);
        }
        catch (...) {
            throw_error(errc::fail_to_monitor_mount);
        }

        for (auto& top_dir: *result) {
            top_dir.add_observer(result);
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

top_directory_set::
top_directory_set()
{
    try {
        for (auto const& top_dir: env().mounted_top_directories()) {
            try {
                auto [_, ok] = emplace_dir(m_top_dirs, top_dir);
                if (!ok) {
                    log::warn() << "try to create duplicated top_directory: "
                                << top_dir << std::endl;
                }
            }
            catch (...) {
                auto const ec = err::current_error_code();

                if (ec == errc::fail_to_monitor_directory) {
                    // ignore (permission denied etc)
                }
                else {
                    print_error(log::warn());
                }
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

top_directory_set::const_iterator top_directory_set::
begin() const noexcept
{
    return m_top_dirs.begin();
}

top_directory_set::const_iterator top_directory_set::
end() const noexcept
{
    return m_top_dirs.end();
}

top_directory_set::iterator top_directory_set::
begin() noexcept
{
    return m_top_dirs.begin();
}

top_directory_set::iterator top_directory_set::
end() noexcept
{
    return m_top_dirs.end();
}

top_directory_set::const_iterator top_directory_set::
find(string_view p) const noexcept
{
    auto const& top_dir = env().top_directory(p);

    return const_iterator {
        find_dir(m_top_dirs, top_dir)
    };
}

top_directory_set::iterator top_directory_set::
find(string_view p) noexcept
{
    auto const& top_dir = env().top_directory(p);

    return find_dir(m_top_dirs, top_dir);
}

top_directory_set::const_iterator top_directory_set::
find(trash_directory const& trash_dir) const
{
    using std::ranges::find_if;

    return find_if(m_top_dirs, [&](auto&& top_dir) {
        return top_dir->contains(trash_dir);
    });
}

top_directory_set::iterator top_directory_set::
insert(string_view p)
{
    try {
        auto [it, ok] = emplace_dir(m_top_dirs, p);
        if (!ok) {
            log::warn() << "try to create duplicated top_directory" << std::endl;

            return m_top_dirs.end();
        }
        else {
            (*it)->add_observer(this->weak_from_this());

            m_observers.for_each([&](auto& ob) {
                ob.top_directory_added(**it);
            });

            return it;
        }
    }
    catch (...) {
        rethrow_error({ { "path", p } });
    }
}

top_directory_set::iterator top_directory_set::
erase(string_view p)
{
    try {
        auto const it = find(p);
        if (it != end()) {

            m_observers.for_each([&](auto& ob) {
                ob.top_directory_about_to_be_removed(*it);
            });

            m_top_dirs.erase(it.base());
        }

        return it;
    }
    catch (...) {
        rethrow_error({ { "path", p } });
    }
}

void top_directory_set::
add_observer(weak_node<observer> const o)
{
    try {
        m_observers.insert(o);
    }
    catch (...) {
        rethrow_error();
    }
}

void top_directory_set::
remove_observer(weak_node<observer> const o)
{
    try {
        m_observers.erase(o);
    }
    catch (...) {
        rethrow_error();
    }
}

void top_directory_set::
filesystem_mounted(string_view mount_point) noexcept
{
    try {
        auto [it, ok] = emplace_dir(m_top_dirs, mount_point);
        if (!ok) {
            log::warn() << "try to create duplicated top_directory" << std::endl;
        }

        (*it)->add_observer(this->weak_from_this());

        m_observers.for_each([&](auto& ob) {
            ob.top_directory_added(*(*it));
        });
    }
    catch (...) {
        print_error(log::warn());
    }
}

void top_directory_set::
filesystem_unmounted(string_view mount_point) noexcept
{
    try {
        auto const it = find(mount_point);
        if (it != end()) {

            m_observers.for_each([&](auto& ob) {
                ob.top_directory_about_to_be_removed(*it);
            });

            m_top_dirs.erase(it.base());
        }
    }
    catch (...) {
        print_error(log::warn());
    }
}

void top_directory_set::
directory_disappeared(top_directory& d)
{
    try {
        m_observers.for_each([&](auto& ob) {
            ob.top_directory_about_to_be_removed(d);
        });

        erase_dir(m_top_dirs, d);
    }
    catch (...) {
        print_error(log::warn());
    }
}

void
tag_invoke(json::value_from_tag,
           json::value& v, top_directory_set const& s)
{
    auto& arr = v.emplace_array();

    for (auto& d: s) {
        arr.push_back(d);
    }
}

} // namespace stream9::xdg::trash
